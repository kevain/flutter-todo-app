# To-Do application
Homework for the Hybrid Mobile Applications (ICD0018) course @ TalTech IT College.

A simple TO-DO Application to keep track of things you need to do. 

Key features:

* Add tasks, separate them into different categories & priority levels

* Support for light & dark mode (theme)

* Local database (SQLite) 

* Register an account and sync with backend (API provided by lecturer)


## Screenshots

App main view - shows a list of tasks. Filtering options in sidebar. Tasks can be swiped to delete them. 
![App main view](./screenshots/screenshot-main.jpg)

Sidebar view - filter tasks by due date, category, priority. 

![App sidebar view](./screenshots/screenshot-sidebar.jpg)