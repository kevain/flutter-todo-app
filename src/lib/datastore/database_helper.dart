import 'dart:async';
import 'dart:io';
import 'package:jiffy/jiffy.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo_app/datastore/DTO/todo_category_with_task_count.dart';
import 'package:todo_app/datastore/DTO/todo_priority_with_task_count.dart';
import 'package:todo_app/datastore/database_model.dart';
import 'package:todo_app/models/app_user.dart';
import 'package:todo_app/models/todo_category.dart';
import 'package:todo_app/models/todo_priority.dart';
import 'package:todo_app/models/todo_task.dart';

class DatabaseHelper {
  static final _databaseName = "todoAppDb.db";
  static final _databaseVersion = 20;

  DatabaseHelper._interbal();
  static final DatabaseHelper instance = DatabaseHelper._interbal();
  factory DatabaseHelper() {
    return instance;
  }

  static Database _database;
  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }

    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate, onUpgrade: _onUpgrade);
  }

  Future<void> _onCreate(Database db, int version) async {
    await db.rawQuery("PRAGMA foreign_keys = ON");

    await db.execute(AppUserTable.CREATE_TABLE_QUERY);

    await db.execute(TodoCategoryTable.CREATE_TABLE_QUERY);
    await db.execute(TodoPriorityTable.CREATE_TABLE_QUERY);
    await db.execute(TodoTaskTable.CREATE_TABLE_QUERY);
  }

  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    await db.execute("DROP TABLE ${TodoCategoryTable.TABLE_NAME};");
    await db.execute("DROP TABLE ${TodoPriorityTable.TABLE_NAME};");
    await db.execute("DROP TABLE ${TodoTaskTable.TABLE_NAME};");
    await db.execute("DROP TABLE ${AppUserTable.TABLE_NAME}");
    await _onCreate(db, newVersion);
  }

  Future<int> insertCategory(TodoCategory todoCategory) async {
    var map = todoCategory.getMapForDbQuery();

    Database db = await instance.database;

    return await db.insert(TodoCategoryTable.TABLE_NAME, map);
  }

  Future<int> insertPriority(TodoPriority todoPriority) async {
    var map = todoPriority.getMapForDbQuery();

    Database db = await instance.database;

    return await db.insert(TodoPriorityTable.TABLE_NAME, map);
  }

  Future<int> insertTask(TodoTask todoTask) async {
    var map = todoTask.getMapForDbQuery();

    Database db = await instance.database;

    return await db.insert(TodoTaskTable.TABLE_NAME, map);
  }

  Future<List<TodoCategory>> getAllCategories() async {
    List<TodoCategory> categories = List<TodoCategory>();

    Database db = await instance.database;

    await db
        .query(TodoCategoryTable.TABLE_NAME,
            where: '${TodoCategoryTable.COLUMN_TO_BE_DELETED} = 0',
            orderBy: '${TodoCategoryTable.COLUMN_CAT_SORT} ASC')
        .then((rows) => {
              for (var row in rows)
                {categories.add(TodoCategory.fromDbRow(row))}
            });

    return categories;
  }

  Future<List<TodoTask>> getAllTodoTasks(String where) async {
    List<TodoTask> todoTasks = List<TodoTask>();

    Database db = await instance.database;

    String orderBy = '''
                ${TodoTaskTable.COLUMN_TASK_SORT} ASC, 
                ${TodoTaskTable.COLUMN_TASK_COMPLETED} ASC, 
                DATETIME(${TodoTaskTable.COLUMN_TASK_DUE}) ASC
    ''';

    await db
        .query(TodoTaskTable.TABLE_NAME, orderBy: orderBy, where: where)
        .then((rows) => {
              for (var row in rows) {todoTasks.add(TodoTask.fromDbRow(row))}
            });

    return todoTasks;
  }

  Future<List<TodoTask>> getTodoTasksByCategoryId(
      int categoryId, String where) async {
    List<TodoTask> todoTasks = List<TodoTask>();

    Database db = await instance.database;
    List<String> columns = [
      TodoTaskTable.COLUMN_TASK_ID,
      TodoTaskTable.COLUMN_TASK_NAME,
      TodoTaskTable.COLUMN_TASK_CAT_ID,
      TodoTaskTable.COLUMN_TASK_PRIORITY_ID,
      TodoTaskTable.COLUMN_TASK_CREATED,
      TodoTaskTable.COLUMN_TASK_DUE,
      TodoTaskTable.COLUMN_TASK_SORT,
      TodoTaskTable.COLUMN_TASK_COMPLETED,
      TodoTaskTable.COLUMN_TASK_EXTERNAL_ID,
      TodoTaskTable.COLUMN_TASK_MODIFIED,
      TodoTaskTable.COLUMN_TASK_ARCHIVED
    ];

    String whereClause = "${TodoTaskTable.COLUMN_TASK_CAT_ID} = ? AND $where";
    List<dynamic> whereArguments = [categoryId];

    String orderBy = ''' 
                ${TodoTaskTable.COLUMN_TASK_SORT} ASC, 
                ${TodoTaskTable.COLUMN_TASK_COMPLETED} ASC, 
                DATETIME(${TodoTaskTable.COLUMN_TASK_DUE}) ASC
    ''';

    await db
        .query(TodoTaskTable.TABLE_NAME,
            columns: columns,
            where: whereClause,
            whereArgs: whereArguments,
            orderBy: orderBy)
        .then((rows) =>
            {rows.forEach((row) => todoTasks.add(TodoTask.fromDbRow(row)))});

    return todoTasks;
  }

  Future<List<TodoCategoryWithTaskCount>>
      getTodoCategoriesWithTaskCounts() async {
    List<TodoCategoryWithTaskCount> list = List<TodoCategoryWithTaskCount>();

    var db = await instance.database;
    var sql = '''SELECT 
    c.${TodoCategoryTable.COLUMN_CAT_ID}, 
    c.${TodoCategoryTable.COLUMN_CAT_NAME},
    c.${TodoCategoryTable.COLUMN_CAT_SORT},
    c.${TodoCategoryTable.COLUMN_EXTERNAL_ID},
    c.${TodoCategoryTable.COLUMN_HAS_BEEN_MODIFIED},
    c.${TodoCategoryTable.COLUMN_TO_BE_DELETED},
    COUNT(t.${TodoTaskTable.COLUMN_TASK_ID}) AS TASKCOUNT
    FROM 
    (SELECT * FROM ${TodoCategoryTable.TABLE_NAME} WHERE ${TodoCategoryTable.COLUMN_TO_BE_DELETED} = 0) c
    LEFT JOIN ${TodoTaskTable.TABLE_NAME} t
    ON c.${TodoCategoryTable.COLUMN_CAT_ID} = t.${TodoTaskTable.COLUMN_TASK_CAT_ID} 
    AND
    ${TodoTaskTable.COLUMN_TASK_COMPLETED} = 0
    AND 
    ${TodoTaskTable.COLUMN_TASK_ARCHIVED} = 0
    GROUP BY c.${TodoCategoryTable.COLUMN_CAT_ID}
    ORDER BY c.${TodoCategoryTable.COLUMN_CAT_SORT}, c.${TodoCategoryTable.COLUMN_CAT_NAME};
    ''';

    await db.rawQuery(sql).then((rows) => rows.forEach((row) => list.add(
          TodoCategoryWithTaskCount.fromDbRow(row),
        )));

    return list;
  }

  Future<Map<String, dynamic>>
      getUncompletedTaskCountsForTodayUpcomingAndAll() async {
    var db = await instance.database;

    var jiffy = Jiffy();

    var todaysDate = jiffy.local().toIso8601String();
    var dateInAWeek = jiffy.add(days: 7).toIso8601String();

    var sql = '''
    SELECT
    *
    FROM
    (
        SELECT 
        COUNT(*) as TASKCOUNT_ALL 
        FROM ${TodoTaskTable.TABLE_NAME} 
        WHERE ${TodoTaskTable.COLUMN_TASK_COMPLETED} = 0 AND ${TodoTaskTable.COLUMN_TASK_ARCHIVED} = 0
    ),
    (
      SELECT
        COUNT(*) AS TASKCOUNT_WEEK
        FROM ${TodoTaskTable.TABLE_NAME}
        WHERE 
        ${TodoTaskTable.COLUMN_TASK_COMPLETED} = 0
        AND
        ${TodoTaskTable.COLUMN_TASK_ARCHIVED} = 0
        AND
        (
          ${TodoTaskTable.COLUMN_TASK_DUE} IS NULL 
        OR 
          DATETIME(${TodoTaskTable.COLUMN_TASK_DUE}) <= DATETIME('$dateInAWeek')
        )
    ),
    (
        SELECT
        COUNT(*) AS TASKCOUNT_TODAY
        FROM ${TodoTaskTable.TABLE_NAME}
        WHERE 
        ${TodoTaskTable.COLUMN_TASK_COMPLETED} = 0
        AND
        ${TodoTaskTable.COLUMN_TASK_ARCHIVED} = 0
        AND
        (
          ${TodoTaskTable.COLUMN_TASK_DUE} IS NULL 
        OR 
          DATETIME(${TodoTaskTable.COLUMN_TASK_DUE}) <= DATETIME('$todaysDate')
        )
    );   
    ''';

    var result = await db.rawQuery(sql);

    return result[0];
  }

  Future<int> updateTodoTask(TodoTask todoTask) async {
    Database db = await instance.database;

    var map = todoTask.getMapForDbQuery();
    map.remove(TodoTaskTable.COLUMN_TASK_ID); // do not update primary key!

    return await db.update(TodoTaskTable.TABLE_NAME, map,
        where: '${TodoTaskTable.COLUMN_TASK_ID} = ? ',
        whereArgs: [todoTask.taskId]);
  }

  Future<int> removeTodoTask(int taskId) async {
    Database db = await instance.database;

    return await db.delete(TodoTaskTable.TABLE_NAME,
        where: "${TodoTaskTable.COLUMN_TASK_ID} = ?", whereArgs: [taskId]);
  }

  Future<int> removeCategory(int categoryId) async {
    Database db = await instance.database;

    await db.delete(TodoTaskTable.TABLE_NAME,
        where: "${TodoTaskTable.COLUMN_TASK_CAT_ID} = ?",
        whereArgs: [categoryId]);

    return await db.delete(TodoCategoryTable.TABLE_NAME,
        where: "${TodoCategoryTable.COLUMN_CAT_ID} = ?",
        whereArgs: [categoryId]);
  }

  Future<int> updateTodoCategory(TodoCategory todoCategory) async {
    Database db = await instance.database;

    var map = todoCategory.getMapForDbQuery();
    map.remove(TodoCategoryTable.COLUMN_CAT_ID); // do not update primary key!

    return await db.update(TodoCategoryTable.TABLE_NAME, map,
        where: '${TodoCategoryTable.COLUMN_CAT_ID} = ? ',
        whereArgs: [todoCategory.categoryId]);
  }

  Future<List<TodoTask>> getTodoTasksForToday(String where) async {
    Database db = await instance.database;

    List<TodoTask> todoTasks = List<TodoTask>();

    String todayDate = DateTime.now().toIso8601String();

    List<String> columns = [
      TodoTaskTable.COLUMN_TASK_ID,
      TodoTaskTable.COLUMN_TASK_NAME,
      TodoTaskTable.COLUMN_TASK_CAT_ID,
      TodoTaskTable.COLUMN_TASK_PRIORITY_ID,
      TodoTaskTable.COLUMN_TASK_CREATED,
      TodoTaskTable.COLUMN_TASK_DUE,
      TodoTaskTable.COLUMN_TASK_SORT,
      TodoTaskTable.COLUMN_TASK_COMPLETED,
      TodoTaskTable.COLUMN_TASK_EXTERNAL_ID,
      TodoTaskTable.COLUMN_TASK_MODIFIED,
      TodoTaskTable.COLUMN_TASK_ARCHIVED
    ];

    String whereClause = '''
          $where
          AND
          (${TodoTaskTable.COLUMN_TASK_DUE} IS NULL 
          OR 
          DATETIME(${TodoTaskTable.COLUMN_TASK_DUE}) <= DATETIME(?))
          ''';
    List<dynamic> whereArguments = [todayDate];

    String orderBy = ''' 
                ${TodoTaskTable.COLUMN_TASK_SORT} ASC, 
                ${TodoTaskTable.COLUMN_TASK_COMPLETED} ASC, 
                DATETIME(${TodoTaskTable.COLUMN_TASK_DUE}) ASC
    ''';

    await db
        .query(TodoTaskTable.TABLE_NAME,
            columns: columns,
            where: whereClause,
            whereArgs: whereArguments,
            orderBy: orderBy)
        .then((rows) =>
            {rows.forEach((row) => todoTasks.add(TodoTask.fromDbRow(row)))});

    return todoTasks;
  }

  Future<List<TodoTask>> getUpcomingTodoTasks(String where) async {
    Database db = await instance.database;

    List<TodoTask> todoTasks = List<TodoTask>();

    String dateInAWeek = Jiffy().add(days: 7).toIso8601String();

    List<String> columns = [
      TodoTaskTable.COLUMN_TASK_ID,
      TodoTaskTable.COLUMN_TASK_NAME,
      TodoTaskTable.COLUMN_TASK_CAT_ID,
      TodoTaskTable.COLUMN_TASK_PRIORITY_ID,
      TodoTaskTable.COLUMN_TASK_CREATED,
      TodoTaskTable.COLUMN_TASK_DUE,
      TodoTaskTable.COLUMN_TASK_SORT,
      TodoTaskTable.COLUMN_TASK_COMPLETED,
      TodoTaskTable.COLUMN_TASK_EXTERNAL_ID,
      TodoTaskTable.COLUMN_TASK_MODIFIED,
      TodoTaskTable.COLUMN_TASK_ARCHIVED
    ];

    String whereClause = '''
          $where
          AND
          (${TodoTaskTable.COLUMN_TASK_DUE} IS NULL 
          OR 
          DATETIME(${TodoTaskTable.COLUMN_TASK_DUE}) <= DATETIME(?))
          ''';
    List<dynamic> whereArguments = [dateInAWeek];

    String orderBy = ''' 
                ${TodoTaskTable.COLUMN_TASK_SORT} ASC, 
                ${TodoTaskTable.COLUMN_TASK_COMPLETED} ASC, 
                DATETIME(${TodoTaskTable.COLUMN_TASK_DUE}) ASC
    ''';

    await db
        .query(TodoTaskTable.TABLE_NAME,
            columns: columns,
            where: whereClause,
            whereArgs: whereArguments,
            orderBy: orderBy)
        .then((rows) =>
            {rows.forEach((row) => todoTasks.add(TodoTask.fromDbRow(row)))});

    return todoTasks;
  }

  Future<List<TodoCategory>> getAllTodoCategoriesForSyncing() async {
    var categories = List<TodoCategory>();

    var db = await instance.database;

    await db.query(TodoCategoryTable.TABLE_NAME).then((rows) =>
        rows.forEach((row) => categories.add(TodoCategory.fromDbRow(row))));

    return categories;
  }

  Future<List<TodoPriority>> getAllTodoPrioritiesForSyncing() async {
    var priorities = List<TodoPriority>();

    var db = await instance.database;

    await db.query(TodoPriorityTable.TABLE_NAME).then((rows) =>
        rows.forEach((row) => priorities.add(TodoPriority.fromDbRow(row))));

    return priorities;
  }

  Future<int> updateTodoPriority(TodoPriority todoPriority) async {
    Database db = await instance.database;

    var map = todoPriority.getMapForDbQuery();
    map.remove(
        TodoPriorityTable.COLUMN_PRIORITY_ID); // do not update primary key!

    return await db.update(TodoPriorityTable.TABLE_NAME, map,
        where: '${TodoPriorityTable.COLUMN_PRIORITY_ID} = ? ',
        whereArgs: [todoPriority.priorityId]);
  }

  Future<int> removePriority(int priorityId) async {
    Database db = await instance.database;

    await db.delete(TodoTaskTable.TABLE_NAME,
        where: "${TodoTaskTable.COLUMN_TASK_PRIORITY_ID} = ?",
        whereArgs: [priorityId]);

    return await db.delete(TodoPriorityTable.TABLE_NAME,
        where: "${TodoPriorityTable.COLUMN_PRIORITY_ID} = ?",
        whereArgs: [priorityId]);
  }

  Future<List<TodoTask>> getAllTodoTasksForSyncing() async {
    List<TodoTask> todotasks = List<TodoTask>();

    Database db = await instance.database;

    await db.query(TodoTaskTable.TABLE_NAME).then((rows) =>
        rows.forEach((row) => todotasks.add(TodoTask.fromDbRow(row))));

    return todotasks;
  }

  Future<TodoCategory> getTodoCategoryById(int categoryId) async {
    Database db = await instance.database;

    TodoCategory todoCategory;

    await db.query(
      TodoCategoryTable.TABLE_NAME,
      where: '${TodoCategoryTable.COLUMN_CAT_ID} = ?',
      whereArgs: [categoryId],
    ).then((result) {
      todoCategory = TodoCategory.fromDbRow(result.first);
    });

    return todoCategory;
  }

  Future<TodoPriority> getTodoPriorityById(int priorityId) async {
    Database db = await instance.database;

    TodoPriority todoPriority;

    await db.query(TodoPriorityTable.TABLE_NAME,
        where: '${TodoPriorityTable.COLUMN_PRIORITY_ID} = ?',
        whereArgs: [priorityId]).then((result) {
      todoPriority = TodoPriority.fromDbRow(result.first);
    });

    return todoPriority;
  }

  Future<TodoCategory> getTodoCategoryByExternalId(int externalId) async {
    Database db = await instance.database;

    TodoCategory todoCategory;

    await db.query(TodoCategoryTable.TABLE_NAME,
        where: '${TodoCategoryTable.COLUMN_EXTERNAL_ID} = ?',
        whereArgs: [externalId]).then((result) {
      todoCategory = TodoCategory.fromDbRow(result.first);
    });

    return todoCategory;
  }

  Future<TodoPriority> getTodoPriorityByExternalId(int externalId) async {
    Database db = await instance.database;

    TodoPriority todoPriority;

    await db.query(TodoPriorityTable.TABLE_NAME,
        where: '${TodoPriorityTable.COLUMN_EXTERNAL_ID} = ?',
        whereArgs: [externalId]).then((result) {
      todoPriority = TodoPriority.fromDbRow(result.first);
    });

    return todoPriority;
  }

  Future<void> insertAppUser(AppUser appUser) async {
    Database db = await instance.database;

    await db.insert(AppUserTable.TABLE_NAME, appUser.getMapForDbQuery());
  }

  Future<void> removeAppUser() async {
    Database db = await instance.database;

    await db.delete(AppUserTable.TABLE_NAME);
  }

  Future<AppUser> getAppUser() async {
    Database db = await instance.database;

    var query = await db.query(AppUserTable.TABLE_NAME);

    if (query.isEmpty || query == null) {
      return null;
    }

    return AppUser.fromDbRow(query.first);
  }

  void fillWithInitialData() async {
    Database db = await instance.database;

    TodoCategory demoCategory =
        TodoCategory(categoryName: "First Category", categorySort: 0);
    demoCategory.categoryId = await db.insert(
        TodoCategoryTable.TABLE_NAME, demoCategory.getMapForDbQuery());

    TodoPriority demoPriority =
        TodoPriority(priorityName: "First Priority", prioritySort: 0);
    demoPriority.priorityId = await db.insert(
        TodoPriorityTable.TABLE_NAME, demoPriority.getMapForDbQuery());

    TodoTask demoTaskUncompleted = TodoTask(
        taskName: "Get to know todoApp",
        taskCreated: DateTime.now(),
        taskDue: DateTime.now(),
        taskSort: 0,
        taskCategoryId: demoCategory.categoryId,
        taskPriorityId: demoPriority.priorityId);
    demoTaskUncompleted.taskId = await db.insert(
        TodoTaskTable.TABLE_NAME, demoTaskUncompleted.getMapForDbQuery());

    TodoTask demoTaskCompleted = TodoTask(
        taskName: "Install todoApp",
        taskCreated: DateTime.now(),
        taskCompleted: true,
        taskSort: 0,
        taskCategoryId: demoCategory.categoryId,
        taskPriorityId: demoPriority.priorityId);
    demoTaskCompleted.taskId = await db.insert(
        TodoTaskTable.TABLE_NAME, demoTaskCompleted.getMapForDbQuery());
  }

  void removeUserData() async {
    var db = await instance.database;

    await db.delete(TodoTaskTable.TABLE_NAME);
    await db.delete(TodoPriorityTable.TABLE_NAME);
    await db.delete(TodoCategoryTable.TABLE_NAME);
  }

  Future<List<TodoPriority>> getAllTodoPriorities() async {
    var db = await instance.database;

    List<TodoPriority> todoPriorities = List();

    await db
        .query(TodoPriorityTable.TABLE_NAME,
            where: '${TodoPriorityTable.COLUMN_TO_BE_DELETED} = 0',
            orderBy: '${TodoPriorityTable.COLUMN_PRIORITY_SORT} ASC')
        .then((rows) => rows
            .forEach((row) => todoPriorities.add(TodoPriority.fromDbRow(row))));

    return todoPriorities;
  }

  Future<List<TodoTask>> getTodoTasksByPriority(
      TodoPriority todoPriority, String where) async {
    var tasks = List<TodoTask>();

    var db = await instance.database;

    await db.query(TodoTaskTable.TABLE_NAME,
        where: '${TodoTaskTable.COLUMN_TASK_PRIORITY_ID} = ? AND $where',
        whereArgs: [
          todoPriority.priorityId
        ]).then(
        (rows) => rows.forEach((row) => tasks.add(TodoTask.fromDbRow(row))));

    return tasks;
  }

  Future<List<TodoPriorityWithTaskCount>>
      getTodoPrioritiesWithTaskCounts() async {
    List<TodoPriorityWithTaskCount> list = List<TodoPriorityWithTaskCount>();

    var db = await instance.database;
    var sql = '''SELECT 
    c.${TodoPriorityTable.COLUMN_PRIORITY_ID}, 
    c.${TodoPriorityTable.COLUMN_PRIORITY_NAME},
    c.${TodoPriorityTable.COLUMN_PRIORITY_SORT},
    c.${TodoPriorityTable.COLUMN_EXTERNAL_ID},
    c.${TodoPriorityTable.COLUMN_HAS_BEEN_MODIFIED},
    c.${TodoPriorityTable.COLUMN_TO_BE_DELETED},
    COUNT(t.${TodoTaskTable.COLUMN_TASK_ID}) AS TASKCOUNT
    FROM 
    (SELECT * FROM ${TodoPriorityTable.TABLE_NAME} WHERE ${TodoPriorityTable.COLUMN_TO_BE_DELETED} = 0) c
    LEFT JOIN ${TodoTaskTable.TABLE_NAME} t
    ON c.${TodoPriorityTable.COLUMN_PRIORITY_ID} = t.${TodoTaskTable.COLUMN_TASK_PRIORITY_ID} 
    AND
    ${TodoTaskTable.COLUMN_TASK_COMPLETED} = 0
    AND 
    ${TodoTaskTable.COLUMN_TASK_ARCHIVED} = 0
    GROUP BY c.${TodoPriorityTable.COLUMN_PRIORITY_ID}
    ORDER BY c.${TodoPriorityTable.COLUMN_PRIORITY_SORT}, c.${TodoPriorityTable.COLUMN_PRIORITY_NAME};
    ''';

    await db.rawQuery(sql).then((rows) => rows.forEach((row) => list.add(
          TodoPriorityWithTaskCount.fromDbRow(row),
        )));

    return list;
  }
}
