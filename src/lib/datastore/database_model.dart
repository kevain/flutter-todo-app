class TodoCategoryTable {
  static const TABLE_NAME = "TodoCategory";
  static const COLUMN_CAT_ID = "categoryId";
  static const COLUMN_CAT_NAME = "categoryName";
  static const COLUMN_CAT_SORT = "categorySort";
  static const COLUMN_EXTERNAL_ID = "categoryExternalId";
  static const COLUMN_HAS_BEEN_MODIFIED = "hasBeenModified";
  static const COLUMN_TO_BE_DELETED = "toBeDeleted";

  static const CREATE_TABLE_QUERY = '''
        CREATE TABLE $TABLE_NAME (
            $COLUMN_CAT_ID INTEGER PRIMARY KEY AUTOINCREMENT,
            $COLUMN_CAT_NAME TEXT NOT NULL,
            $COLUMN_CAT_SORT INT NOT NULL,
            $COLUMN_EXTERNAL_ID INT,
            $COLUMN_HAS_BEEN_MODIFIED INT NOT NULL DEFAULT 0,
            $COLUMN_TO_BE_DELETED INT NOT NULL DEFAULT 0
      );
  ''';
}

class TodoPriorityTable {
  static const TABLE_NAME = "TodoPriority";
  static const COLUMN_PRIORITY_ID = "priorityId";
  static const COLUMN_PRIORITY_NAME = "priorityName";
  static const COLUMN_PRIORITY_SORT = "prioritySort";
  static const COLUMN_EXTERNAL_ID = "priorityExternalId";
  static const COLUMN_HAS_BEEN_MODIFIED = "hasBeenModified";
  static const COLUMN_TO_BE_DELETED = "toBeDeleted";

  static const CREATE_TABLE_QUERY = '''
        CREATE TABLE $TABLE_NAME (
            $COLUMN_PRIORITY_ID INTEGER PRIMARY KEY AUTOINCREMENT,
            $COLUMN_PRIORITY_NAME TEXT NOT NULL,
            $COLUMN_PRIORITY_SORT INT NOT NULL,
            $COLUMN_EXTERNAL_ID INT,
            $COLUMN_HAS_BEEN_MODIFIED INT NOT NULL DEFAULT 0,
            $COLUMN_TO_BE_DELETED INT NOT NULL DEFAULT 0
      );
  ''';
}

class TodoTaskTable {
  static const TABLE_NAME = "TodoTask";
  static const COLUMN_TASK_ID = "taskId";
  static const COLUMN_TASK_NAME = "taskName";
  static const COLUMN_TASK_CAT_ID = "taskCategoryId";
  static const COLUMN_TASK_PRIORITY_ID = "taskPriorityId";
  static const COLUMN_TASK_SORT = "taskSort";
  static const COLUMN_TASK_CREATED = "taskCreated";
  static const COLUMN_TASK_DUE = "taskDue";
  static const COLUMN_TASK_COMPLETED = "taskCompleted";
  static const COLUMN_TASK_ARCHIVED = "taskArchived";
  static const COLUMN_TASK_EXTERNAL_ID = "taskExternalId";
  static const COLUMN_TASK_MODIFIED = "taskModified";

  static const CREATE_TABLE_QUERY = '''
        CREATE TABLE TodoTask (
            $COLUMN_TASK_ID INTEGER PRIMARY KEY AUTOINCREMENT,
            $COLUMN_TASK_NAME TEXT NOT NULL,
            
            $COLUMN_TASK_CAT_ID INT REFERENCES TodoCategory(categoryId),
            $COLUMN_TASK_PRIORITY_ID INT REFERENCES TodoPriority(priorityId),

            $COLUMN_TASK_SORT INT NOT NULL,
            $COLUMN_TASK_CREATED TEXT DEFAULT (DATE('NOW')),
            $COLUMN_TASK_DUE TEXT,

            $COLUMN_TASK_COMPLETED INT NOT NULL DEFAULT 0,
            $COLUMN_TASK_ARCHIVED INT NOT NULL DEFAULT 0,

            $COLUMN_TASK_EXTERNAL_ID INT,
            $COLUMN_TASK_MODIFIED INT NOT NULL DEFAULT 0,

            FOREIGN KEY(${TodoTaskTable.COLUMN_TASK_CAT_ID}) REFERENCES ${TodoCategoryTable.TABLE_NAME}(${TodoCategoryTable.COLUMN_CAT_ID}),
            FOREIGN KEY(${TodoTaskTable.COLUMN_TASK_PRIORITY_ID}) REFERENCES ${TodoPriorityTable.TABLE_NAME}(${TodoPriorityTable.COLUMN_PRIORITY_ID})
      );
  ''';
}

class AppUserTable {
  static const TABLE_NAME = 'AppUser';
  static const COLUMN_EMAIL = 'email';
  static const COLUMN_PASSWORD = 'password';

  static const CREATE_TABLE_QUERY = '''
        CREATE TABLE $TABLE_NAME (
          $COLUMN_EMAIL TEXT,
          $COLUMN_PASSWORD TEXT
        );
  ''';
}

class TodoTasksFilter {
  static const ALL = '${TodoTaskTable.COLUMN_TASK_ARCHIVED} = 0';
  static const DONE =
      '${TodoTaskTable.COLUMN_TASK_ARCHIVED} = 0 AND ${TodoTaskTable.COLUMN_TASK_COMPLETED} = 1';
  static const NOT_DONE =
      '${TodoTaskTable.COLUMN_TASK_ARCHIVED} = 0 AND ${TodoTaskTable.COLUMN_TASK_COMPLETED} = 0';
}
