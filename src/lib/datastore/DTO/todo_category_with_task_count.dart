import 'package:todo_app/models/todo_category.dart';

class TodoCategoryWithTaskCount {
  final TodoCategory todoCategory;
  final int taskCount;

  TodoCategoryWithTaskCount(this.todoCategory, this.taskCount);

  factory TodoCategoryWithTaskCount.fromDbRow(Map<String, dynamic> row) {
    return TodoCategoryWithTaskCount(
        TodoCategory.fromDbRow(row), row["TASKCOUNT"]);
  }
}
