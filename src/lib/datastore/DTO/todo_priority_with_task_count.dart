import 'package:todo_app/models/todo_priority.dart';

class TodoPriorityWithTaskCount {
  final TodoPriority todoPriority;
  final int taskCount;

  TodoPriorityWithTaskCount(this.todoPriority, this.taskCount);

  factory TodoPriorityWithTaskCount.fromDbRow(Map<String, dynamic> row) {
    return TodoPriorityWithTaskCount(
        TodoPriority.fromDbRow(row), row["TASKCOUNT"]);
  }
}
