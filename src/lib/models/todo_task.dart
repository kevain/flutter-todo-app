import 'package:flutter/material.dart';
import 'package:todo_app/API/api_json_structure.dart';
import 'package:todo_app/datastore/database_helper.dart';
import 'package:todo_app/datastore/database_model.dart';

class TodoTask {
  int taskId;
  String taskName;
  int taskCategoryId;
  int taskPriorityId;
  int taskSort;
  DateTime taskCreated;
  DateTime taskDue;
  bool taskCompleted;
  bool taskArchived;
  int taskExternalId;
  bool taskModified;

  TodoTask(
      {this.taskId,
      @required this.taskName,
      @required this.taskCategoryId,
      @required this.taskPriorityId,
      this.taskSort = 0,
      @required this.taskCreated,
      this.taskDue,
      this.taskCompleted = false,
      this.taskArchived = false,
      this.taskExternalId,
      this.taskModified = false});

  Map<String, dynamic> getMapForDbQuery() {
    return {
      TodoTaskTable.COLUMN_TASK_ID: this.taskId,
      TodoTaskTable.COLUMN_TASK_NAME: this.taskName,
      TodoTaskTable.COLUMN_TASK_CAT_ID: this.taskCategoryId,
      TodoTaskTable.COLUMN_TASK_PRIORITY_ID: this.taskPriorityId,
      TodoTaskTable.COLUMN_TASK_SORT: this.taskSort,
      TodoTaskTable.COLUMN_TASK_CREATED:
          this.taskCreated != null ? this.taskCreated.toIso8601String() : null,
      TodoTaskTable.COLUMN_TASK_DUE:
          this.taskDue != null ? this.taskDue.toIso8601String() : null,
      TodoTaskTable.COLUMN_TASK_COMPLETED: this.taskCompleted ? 1 : 0,
      TodoTaskTable.COLUMN_TASK_ARCHIVED: this.taskArchived ? 1 : 0,
      TodoTaskTable.COLUMN_TASK_EXTERNAL_ID: this.taskExternalId,
      TodoTaskTable.COLUMN_TASK_MODIFIED: this.taskModified ? 1 : 0
    };
  }

  Future<Map<String, dynamic>> getMapForJson() async {
    var db = DatabaseHelper();
    var category = await db.getTodoCategoryById(this.taskCategoryId);
    var priority = await db.getTodoPriorityById(this.taskPriorityId);

    return {
      TodoTaskJson.id: this.taskExternalId,
      TodoTaskJson.name: this.taskName,
      TodoTaskJson.sort: this.taskSort,
      TodoTaskJson.createdDt:
          this.taskCreated != null ? this.taskCreated.toIso8601String() : null,
      TodoTaskJson.dueDt:
          this.taskDue != null ? this.taskDue.toIso8601String() : null,
      TodoTaskJson.isCompleted: this.taskCompleted,
      TodoTaskJson.isArchived: this.taskArchived,
      TodoTaskJson.categoryId: category.categoryExternalId,
      TodoTaskJson.priorityId: priority.priorityExternalId
    };
  }

  factory TodoTask.fromDbRow(Map<String, dynamic> row) {
    return TodoTask(
        taskId: row[TodoTaskTable.COLUMN_TASK_ID],
        taskName: row[TodoTaskTable.COLUMN_TASK_NAME],
        taskCategoryId: row[TodoTaskTable.COLUMN_TASK_CAT_ID],
        taskPriorityId: row[TodoTaskTable.COLUMN_TASK_PRIORITY_ID],
        taskSort: row[TodoTaskTable.COLUMN_TASK_SORT],
        taskCreated: row[TodoTaskTable.COLUMN_TASK_CREATED] != null
            ? DateTime.parse(row[TodoTaskTable.COLUMN_TASK_CREATED])
            : DateTime.now(),
        taskDue: row[TodoTaskTable.COLUMN_TASK_DUE] != null
            ? DateTime.parse(row[TodoTaskTable.COLUMN_TASK_DUE])
            : null,
        taskExternalId: row[TodoTaskTable.COLUMN_TASK_EXTERNAL_ID],
        taskArchived:
            row[TodoTaskTable.COLUMN_TASK_ARCHIVED] == 1 ? true : false,
        taskModified:
            row[TodoTaskTable.COLUMN_TASK_MODIFIED] == 1 ? true : false,
        taskCompleted:
            row[TodoTaskTable.COLUMN_TASK_COMPLETED] == 1 ? true : false);
  }

  factory TodoTask.fromJson(Map<String, dynamic> json) {
    return TodoTask(
        taskExternalId: json[TodoTaskJson.id],
        taskName: json[TodoTaskJson.name],
        taskSort: json[TodoTaskJson.sort],
        taskCreated: DateTime.parse(json[TodoTaskJson.createdDt]),
        taskDue: json[TodoTaskJson.dueDt] != null
            ? DateTime.parse(json[TodoTaskJson.dueDt])
            : null,
        taskCompleted: json[TodoTaskJson.isCompleted],
        taskArchived: json[TodoTaskJson.isArchived],
        taskCategoryId: json[TodoTaskJson.categoryId],
        taskPriorityId: json[TodoTaskJson.priorityId]);
  }

  static String taskNameValidator(String newName) {
    newName = newName.trim();

    if (newName.length < 3) {
      return "Task must be at least 3 characters";
    } else if (newName.length >= 30) {
      return "Task must be shorter than 30 characters";
    } else
      return null;
  }
}
