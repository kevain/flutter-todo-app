import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:todo_app/API/api_json_structure.dart';
import 'package:todo_app/datastore/database_model.dart';

class TodoCategory {
  int categoryId;
  String categoryName;
  int categorySort;
  int categoryExternalId;
  bool categoryHasBeenModified;
  bool categoryToBeDeleted;

  TodoCategory(
      {this.categoryId,
      @required this.categoryName,
      this.categorySort = 0,
      this.categoryExternalId,
      this.categoryHasBeenModified = false,
      this.categoryToBeDeleted = false});

  Map<String, dynamic> getMapForDbQuery() {
    return {
      TodoCategoryTable.COLUMN_CAT_ID: this.categoryId,
      TodoCategoryTable.COLUMN_CAT_NAME: this.categoryName,
      TodoCategoryTable.COLUMN_CAT_SORT: this.categorySort,
      TodoCategoryTable.COLUMN_EXTERNAL_ID: this.categoryExternalId,
      TodoCategoryTable.COLUMN_TO_BE_DELETED: this.categoryToBeDeleted ? 1 : 0,
      TodoCategoryTable.COLUMN_HAS_BEEN_MODIFIED:
          this.categoryHasBeenModified ? 1 : 0
    };
  }

  Map<String, dynamic> getMapForJson() {
    return {
      TodoCategoryJson.id: this.categoryExternalId,
      TodoCategoryJson.name: this.categoryName,
      TodoCategoryJson.sort: this.categorySort
    };
  }

  factory TodoCategory.fromDbRow(Map<String, dynamic> row) {
    return TodoCategory(
        categoryId: row[TodoCategoryTable.COLUMN_CAT_ID],
        categoryName: row[TodoCategoryTable.COLUMN_CAT_NAME],
        categorySort: row[TodoCategoryTable.COLUMN_CAT_SORT],
        categoryExternalId: row[TodoCategoryTable.COLUMN_EXTERNAL_ID],
        categoryToBeDeleted:
            row[TodoCategoryTable.COLUMN_TO_BE_DELETED] == 1 ? true : false,
        categoryHasBeenModified:
            row[TodoCategoryTable.COLUMN_HAS_BEEN_MODIFIED] == 1
                ? true
                : false);
  }

  factory TodoCategory.fromJson(Map<String, dynamic> json) {
    return TodoCategory(
        categoryExternalId: json[TodoCategoryJson.id],
        categoryName: json[TodoCategoryJson.name],
        categorySort: json[TodoCategoryJson.sort]);
  }

  // Validator for TextFields on category creation and editing dialogs
  static String categoryNameValidator(String newName) {
    newName = newName.trim();

    if (newName.length < 3) {
      return "Name must be at least 3 characters";
    } else if (newName.length >= 15) {
      return "Name must be shorter than 15 characters";
    } else
      return null;
  }
}
