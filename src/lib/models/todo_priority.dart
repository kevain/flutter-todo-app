import 'package:flutter/material.dart';
import 'package:todo_app/API/api_json_structure.dart';
import 'package:todo_app/datastore/database_model.dart';

class TodoPriority {
  int priorityId;
  String priorityName;
  int prioritySort;
  int priorityExternalId;
  bool priorityHasBeenModified;
  bool priorityToBeDeleted;

  TodoPriority(
      {this.priorityId,
      @required this.priorityName,
      @required this.prioritySort,
      this.priorityExternalId,
      this.priorityHasBeenModified = false,
      this.priorityToBeDeleted = false});

  Map<String, dynamic> getMapForDbQuery() {
    return {
      TodoPriorityTable.COLUMN_PRIORITY_ID: this.priorityId,
      TodoPriorityTable.COLUMN_PRIORITY_NAME: this.priorityName,
      TodoPriorityTable.COLUMN_PRIORITY_SORT: this.prioritySort,
      TodoPriorityTable.COLUMN_EXTERNAL_ID: this.priorityExternalId,
      TodoPriorityTable.COLUMN_TO_BE_DELETED: this.priorityToBeDeleted ? 1 : 0,
      TodoPriorityTable.COLUMN_HAS_BEEN_MODIFIED:
          this.priorityHasBeenModified ? 1 : 0
    };
  }

  Map<String, dynamic> getMapForJson() {
    return {
      TodoPriorityJson.id: this.priorityExternalId,
      TodoPriorityJson.name: this.priorityName,
      TodoPriorityJson.sort: this.prioritySort
    };
  }

  factory TodoPriority.fromDbRow(Map<String, dynamic> row) {
    return TodoPriority(
        priorityId: row[TodoPriorityTable.COLUMN_PRIORITY_ID],
        priorityName: row[TodoPriorityTable.COLUMN_PRIORITY_NAME],
        prioritySort: row[TodoPriorityTable.COLUMN_PRIORITY_SORT],
        priorityExternalId: row[TodoPriorityTable.COLUMN_EXTERNAL_ID],
        priorityToBeDeleted:
            row[TodoPriorityTable.COLUMN_TO_BE_DELETED] == 1 ? true : false,
        priorityHasBeenModified:
            row[TodoPriorityTable.COLUMN_HAS_BEEN_MODIFIED] == 1
                ? true
                : false);
  }

  factory TodoPriority.fromJson(Map<String, dynamic> json) {
    return TodoPriority(
        priorityExternalId: json[TodoPriorityJson.id],
        priorityName: json[TodoPriorityJson.name],
        prioritySort: json[TodoPriorityJson.sort]);
  }

    // Validator for TextFields on priority creation and editing dialogs
  static String priorityNameValidator(String newName) {
    newName = newName.trim();

    if (newName.length < 3) {
      return "Name must be at least 3 characters";
    } else if (newName.length >= 15) {
      return "Name must be shorter than 15 characters";
    } else
      return null;
  }
}
