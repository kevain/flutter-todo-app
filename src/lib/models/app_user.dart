import 'package:todo_app/datastore/database_model.dart';

class AppUser {
  String email;
  String password;

  AppUser({this.email, this.password});

  Map<String, dynamic> getMapForDbQuery() {
    return {
      AppUserTable.COLUMN_EMAIL: this.email,
      AppUserTable.COLUMN_PASSWORD: this.password
    };
  }

  Map<String, dynamic> getJson() {
    return {'email': this.email, 'password': this.password};
  }

  factory AppUser.fromDbRow(Map<String, dynamic> row) {
    return AppUser(
        email: row[AppUserTable.COLUMN_EMAIL],
        password: row[AppUserTable.COLUMN_PASSWORD]);
  }
}
