import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/todo_category.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/widgets/dialogs/todo_category_dialog.dart';
import 'package:todo_app/widgets/sidebar/sidebar.dart';

class TodoCategoryListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('All Categories'),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
            size: 35,
          ),
          onPressed: () => showDialog(
              context: context, builder: (context) => TodoCategoryDialog()),
        ),
        drawer: SideBar(),
        body: Consumer<TodoProvider>(
          builder: (context, categoryProvider, _) {
            var future = categoryProvider.todoCategories;
            return FutureBuilder(
              future: future,
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                  case ConnectionState.active:
                    return Container();

                  case ConnectionState.done:
                    var items = snapshot.data as List<TodoCategory>;

                    return Container(
                        margin: EdgeInsets.all(1),
                        child: ReorderableListView(
                            onReorder: (int oldIndex, int newIndex) {
                              var cat = items[oldIndex];
                              cat.categorySort = newIndex;
                              cat.categoryHasBeenModified = true;
                              categoryProvider.updateCategory(cat);
                            },
                            children: _generateCategoryTiles(items, context)));
                }
                return null;
              },
            );
          },
        ));
  }

  List<Widget> _generateCategoryTiles(
      List<TodoCategory> categories, BuildContext context) {
    var tiles = List<Widget>();

    categories.forEach((cat) => tiles.add(Padding(
          key: ValueKey('${cat.categoryId}/${cat.categoryName}'),
          child: Card(
              elevation: 5,
              margin: EdgeInsets.all(1),
              child: ListTile(
                title: Text('${cat.categoryName}'),
                trailing: Icon(Icons.list),
                onTap: () => showDialog(
                    context: context,
                    builder: (context) => TodoCategoryDialog(
                          todoCategory: cat,
                        )),
              )),
          padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
        )));

    return tiles;
  }
}
