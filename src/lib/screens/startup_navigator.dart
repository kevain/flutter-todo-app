import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/app_user.dart';
import 'package:todo_app/providers/user_data_provider.dart';
import 'package:todo_app/screens/login_screen.dart';
import 'package:todo_app/screens/todo_task_list_screen.dart';

// Dummy navigator widget. Pushes user past login screen if they were previously logged in.
// in real word, should be done through a service instead.
class StartupNavigator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<UserDataProvider>(context);

    return FutureBuilder(
      future: provider.appUser,
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
          case ConnectionState.active:
            return Center(
              child: CircularProgressIndicator(),
            );
          case ConnectionState.done:
            var appUser = snapshot.data as AppUser;

            if (appUser == null) {
              return LoginScreen();
            } else {
              return TodoTaskViewScreen(
                viewType: TodoTaskViewType.All,
              );
            }
        }

        return null;
      },
    );
  }
}
