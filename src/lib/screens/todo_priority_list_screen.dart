import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/todo_priority.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/widgets/dialogs/todo_priority_dialog.dart';
import 'package:todo_app/widgets/sidebar/sidebar.dart';

class TodoPriorityListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('All Priorities'),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
            size: 35,
          ),
          onPressed: () => showDialog(
              context: context, builder: (context) => TodoPriorityDialog()),
        ),
        drawer: SideBar(),
        body: Consumer<TodoProvider>(
          builder: (context, todoProvider, _) {
            var future = todoProvider.todoPriorities;
            return FutureBuilder(
              future: future,
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                  case ConnectionState.active:
                    return Container();

                  case ConnectionState.done:
                    var items = snapshot.data as List<TodoPriority>;

                    return Container(
                        margin: EdgeInsets.all(1),
                        child: ReorderableListView(
                            onReorder: (int oldIndex, int newIndex) {
                              var priority = items[oldIndex];
                              priority.prioritySort = newIndex;
                              priority.priorityHasBeenModified = true;
                              todoProvider.updateTodoPriority(priority);
                            },
                            children: _generatePriorityTiles(items, context)));
                }
                return null;
              },
            );
          },
        ));
  }

  List<Widget> _generatePriorityTiles(
      List<TodoPriority> priorities, BuildContext context) {
    var tiles = List<Widget>();

    priorities.forEach((priority) => tiles.add(Padding(
          key: ValueKey('${priority.priorityId}/${priority.priorityName}'),
          child: Card(
              elevation: 5,
              margin: EdgeInsets.all(1),
              child: ListTile(
                title: Text('${priority.priorityName}'),
                trailing: Icon(Icons.list),
                onTap: () => showDialog(
                    context: context,
                    builder: (context) => TodoPriorityDialog(
                          todoPriority: priority,
                        )),
              )),
          padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
        )));

    return tiles;
  }
}
