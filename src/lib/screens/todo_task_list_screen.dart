import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/datastore/database_model.dart';
import 'package:todo_app/models/todo_category.dart';
import 'package:todo_app/models/todo_priority.dart';
import 'package:todo_app/models/todo_task.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/widgets/dialogs/todo_task_dialog.dart';
import 'package:todo_app/widgets/lists/task/todo_task_list.dart';
import 'package:todo_app/widgets/sidebar/sidebar.dart';

enum TodoTaskViewType { Category, Priority, Today, Upcoming, All }

class TodoTaskViewScreen extends StatefulWidget {
  final TodoCategory todoCategory;
  final TodoTaskViewType viewType;
  final TodoPriority todoPriority;

  const TodoTaskViewScreen(
      {Key key, this.todoCategory, this.viewType, this.todoPriority})
      : super(key: key);

  @override
  _TodoTaskViewScreenState createState() => _TodoTaskViewScreenState();
}

class _TodoTaskViewScreenState extends State<TodoTaskViewScreen> {
  String todoTaskFilter = TodoTasksFilter.ALL;

  void setTodoTasksFilter(String filter) {
    setState(() {
      todoTaskFilter = filter;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(generateHeader()),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () => Provider.of<TodoProvider>(context).syncData(),
          ),
          PopupMenuButton<String>(
              onSelected: setTodoTasksFilter,
              itemBuilder: (_) => [
                    PopupMenuItem(
                      value: TodoTasksFilter.ALL,
                      child: Text('Show All Tasks'),
                    ),
                    PopupMenuItem(
                      value: TodoTasksFilter.DONE,
                      child: Text('Show Only Done Tasks'),
                    ),
                    PopupMenuItem(
                      value: TodoTasksFilter.NOT_DONE,
                      child: Text('Show Only Uncompleted Tasks'),
                    ),
                  ]),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          size: 35,
        ),
        onPressed: () =>
            showDialog(context: context, builder: (_) => TodoTaskDialog()),
      ),
      drawer: SideBar(),
      body: Consumer<TodoProvider>(
        builder: (context, taskProvider, _) => FutureBuilder(
          future: getTodoTasks(taskProvider),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
              case ConnectionState.active:
                return Center(
                  child: CircularProgressIndicator(),
                );
              case ConnectionState.done:
                var todoTasks = snapshot.data as List<TodoTask>;
                return TodoTaskList(todoTasks: todoTasks);
            }

            return null; // Should never be reached
          },
        ),
      ),
    );
  }

  void onTodoTaskListOrder(int oldIndex, int newIndex) {}

  Future<List<TodoTask>> getTodoTasks(TodoProvider taskProvider) {
    switch (widget.viewType) {
      case TodoTaskViewType.Category:
        return taskProvider.todoTasksForCategory(
            widget.todoCategory, todoTaskFilter);

      case TodoTaskViewType.Today:
        return taskProvider.todoTasksForToday(todoTaskFilter);

      case TodoTaskViewType.Upcoming:
        return taskProvider.todoTasksForUpcomingWeek(todoTaskFilter);

      case TodoTaskViewType.All:
        return taskProvider.todoTasks(todoTaskFilter);

      case TodoTaskViewType.Priority:
        return taskProvider.todoTasksForPriority(
            widget.todoPriority, todoTaskFilter);
        break;
    }

    return null; // should not be reached
  }

  String generateHeader() {
    switch (widget.viewType) {
      case TodoTaskViewType.Category:
        return widget.todoCategory.categoryName;

      case TodoTaskViewType.Today:
        return 'Today';

      case TodoTaskViewType.Upcoming:
        return 'Following 7 days';

      case TodoTaskViewType.All:
        return 'All Tasks';

      case TodoTaskViewType.Priority:
        return widget.todoPriority.priorityName;
    }
    return 'Should be a header here'; // this should not be reached
  }
}
