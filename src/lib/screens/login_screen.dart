import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/API/todo_api.dart';
import 'package:todo_app/datastore/database_helper.dart';
import 'package:todo_app/models/app_user.dart';
import 'package:todo_app/providers/user_data_provider.dart';
import 'package:todo_app/screens/todo_task_list_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController emailTextController;
  TextEditingController passwordTextController;

  String email = '';
  String password = '';

  void emailListener() {
    var emailText = emailTextController.text.trim();

    email = (emailText.isNotEmpty ? emailText : '');
  }

  void passwordListener() {
    var passwordText = passwordTextController.text.trim();

    password = (passwordText.isNotEmpty ? passwordText : '');
  }

  @override
  void dispose() {
    emailTextController.dispose();
    passwordTextController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    emailTextController = TextEditingController();
    emailTextController.addListener(emailListener);

    passwordTextController = TextEditingController();
    passwordTextController.addListener(passwordListener);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        body: Center(
          child: Card(
            margin: EdgeInsets.all(10),
            elevation: 5,
            shape: RoundedRectangleBorder(
              side: BorderSide.none,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextFormField(
                  autofocus: true,
                  controller: emailTextController,
                  decoration: InputDecoration(labelText: 'Enter Email'),
                  keyboardType: TextInputType.emailAddress,
                ),
                Divider(),
                TextFormField(
                  autofocus: true,
                  controller: passwordTextController,
                  decoration: InputDecoration(labelText: 'Enter Password'),
                  obscureText: true,
                ),
                Divider(),
                ButtonBar(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    RaisedButton(
                      child: Text('Register'),
                      onPressed: () async {
                        var appUser = AppUser(email: email, password: password);
                        var provider = Provider.of<UserDataProvider>(context);
                        provider.removeAppUser();

                        // get token
                        var jwt = await TodoAPI().register(appUser);

                        if (jwt.token.isEmpty) {
                          scaffoldKey.currentState.showSnackBar(
                            SnackBar(
                              content: Text(jwt.status),
                            ),
                          );
                        } else {
                          provider.addAppUser(appUser);
                          DatabaseHelper().fillWithInitialData();
                          TodoAPI().syncData();
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (_) => TodoTaskViewScreen(
                                viewType: TodoTaskViewType.All,
                              ),
                            ),
                          );
                        }
                      },
                    ),
                    RaisedButton(
                      color: Theme.of(context).colorScheme.secondaryVariant,
                      child: Text('Login'),
                      onPressed: () async {
                        // create app user
                        var appUser = AppUser(email: email, password: password);
                        var provider = Provider.of<UserDataProvider>(context);
                        provider.removeAppUser();
                        provider.addAppUser(appUser);

                        // get token
                        var jwt = await TodoAPI().login(appUser);

                        if (jwt.token.isEmpty) {
                          scaffoldKey.currentState.showSnackBar(
                            SnackBar(
                              content: Text(jwt.status),
                            ),
                          );
                        } else {
                          TodoAPI().syncData();
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (_) => TodoTaskViewScreen(
                                viewType: TodoTaskViewType.All,
                              ),
                            ),
                          );
                        }
                      },
                    )
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
