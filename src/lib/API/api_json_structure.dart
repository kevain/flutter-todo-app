class TodoPriorityJson {
  static const id = "id";
  static const name = "toDoPriorityName";
  static const sort = "toDoPrioritySort";
}

class TodoCategoryJson {
  static const id = "id";
  static const name = "toDoCategoryName";
  static const sort = "todoCategorySort";
}

class TodoTaskJson {
  static const id = "id";
  static const name = "toDoTaskName";
  static const sort = "toDoTaskSort";
  static const createdDt = "createdDT";
  static const dueDt = "dueDT";
  static const isCompleted = "isCompleted";
  static const isArchived = "isArchived";
  static const categoryId = "todoCategoryId";
  static const priorityId = "toDoPriorityId";
}

class JwtJson {
  static const token = 'token';
  static const status = 'status';
}
