import 'package:flutter/material.dart';
import 'package:todo_app/API/api_json_structure.dart';

class JWT {
  final String token;
  final String status;

  JWT({@required this.token, @required this.status});

  factory JWT.fromJson(Map<String, dynamic> json) {
    return JWT(token: json[JwtJson.token], status: json[JwtJson.status]);
  }
}
