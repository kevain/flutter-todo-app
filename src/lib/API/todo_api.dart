import 'dart:convert';

import 'package:todo_app/API/api_json_structure.dart';
import 'package:todo_app/datastore/database_helper.dart';
import 'package:todo_app/models/app_user.dart';
import 'package:todo_app/models/todo_category.dart';
import 'package:http/http.dart' as http;
import 'package:todo_app/models/todo_priority.dart';
import 'package:todo_app/models/todo_task.dart';

import 'models/jwt.dart';

class TodoAPI {
  static final TodoAPI _api = TodoAPI._internal();
  TodoAPI._internal();

  factory TodoAPI() {
    return _api;
  }

  final DatabaseHelper _db = DatabaseHelper();

  Map<String, String> headers = {
    'Content-Type': 'application/json',
  };

  void setHeader(String token) {
    headers.addAll({'Authorization': 'bearer $token'});
  }

  static const String BASE_URL = "https://taltech.akaver.com/api/";
  static const String ENDPOINT_CATEGORIES = "todocategories";
  static const String ENDPOINT_PRIORITIES = "todopriorities";
  static const String ENDPOINT_TASKS = "todotasks";

  static const String ENDPOINT_ACCOUNT_REGISTER = "account/register";
  static const String ENDPOINT_ACCOUNT_LOGIN = "account/login";

  void syncData() async {
    var appUser = await _db.getAppUser();
    var jwt;

    try {
      jwt = await login(appUser);
    } catch (Exception) {
      return;
    }

    if (jwt.token.isNotEmpty) {
      setHeader(jwt.token);
    } else
      return;

    List<TodoCategory> todoCategories =
        await _db.getAllTodoCategoriesForSyncing();

    for (var category in todoCategories) {
      if (category.categoryExternalId == null) {
        category = await _postTodoCategory(category);
        await _db.updateTodoCategory(category);
      } else if (category.categoryToBeDeleted) {
        var deletingSucceeded = await _deleteTodoCategory(category);

        if (deletingSucceeded) {
          await _db.removeCategory(category.categoryId);
        }
      } else if (category.categoryHasBeenModified) {
        var updatingSuccessful = await _updateTodoCategory(category);

        if (updatingSuccessful) {
          category.categoryHasBeenModified = false;
          await _db.updateTodoCategory(category);
        }
      }
    }

    // get all categories from api and compare to existing db. Add ones that are missing;
    _fetchAndInsertNewCategories(todoCategories);

    List<TodoPriority> todoPriorities =
        await _db.getAllTodoPrioritiesForSyncing();

    for (var priority in todoPriorities) {
      if (priority.priorityExternalId == null) {
        priority = await _postTodoPriority(priority);
        await _db.updateTodoPriority(priority);
      } else if (priority.priorityToBeDeleted) {
        var deletingSucceeded = await _deleteTodoPriority(priority);

        if (deletingSucceeded) {
          await _db.removePriority(priority.priorityId);
        }
      } else if (priority.priorityHasBeenModified) {
        var updatingSuccessful = await _updateTodoPriority(priority);

        if (updatingSuccessful) {
          priority.priorityHasBeenModified = false;
          await _db.updateTodoPriority(priority);
        }
      }
    }

    _fetchAndInsertNewPriorities(todoPriorities);

    List<TodoTask> todoTasks = await _db.getAllTodoTasksForSyncing();

    for (var todoTask in todoTasks) {
      if (todoTask.taskExternalId == null) {
        todoTask = await _postTodoTask(todoTask);
        await _db.updateTodoTask(todoTask);
      } else if (todoTask.taskArchived) {
        var deletingSucceeded = await _deleteTodoTask(todoTask);

        if (deletingSucceeded) {
          await _db.removeTodoTask(todoTask.taskId);
        }
      } else if (todoTask.taskModified) {
        var updatingSuccessful = await _updateTodoTask(todoTask);

        if (updatingSuccessful) {
          todoTask.taskModified = false;
          await _db.updateTodoTask(todoTask);
        }
      }
    }

    _fetchAndInsertNewTasks(todoTasks);
  }

  Future<TodoCategory> _postTodoCategory(TodoCategory todoCategory) async {
    final response = await http.post(BASE_URL + ENDPOINT_CATEGORIES,
        headers: headers, body: jsonEncode(todoCategory.getMapForJson()));

    if (response.statusCode == 201) {
      var responseJson = json.decode(response.body);

      todoCategory.categoryExternalId = responseJson[TodoCategoryJson.id];
    }

    print(response.statusCode);

    return todoCategory;
  }

  Future<bool> _deleteTodoCategory(TodoCategory todoCategory) async {
    final response = await http.delete(
        BASE_URL + ENDPOINT_CATEGORIES + "/${todoCategory.categoryExternalId}",
        headers: headers);

    return response.statusCode == 200 ||
        response.statusCode == 404; // deleting succeeded or item already gone?
  }

  Future<bool> _updateTodoCategory(TodoCategory todoCategory) async {
    final response = await http.put(
        BASE_URL + ENDPOINT_CATEGORIES + "/${todoCategory.categoryExternalId}",
        headers: headers,
        body: jsonEncode(todoCategory.getMapForJson()));

    return response.statusCode == 204 ||
        response.statusCode != 404; 
  }

  Future<List<TodoCategory>> _getTodoCategories() async {
    final response =
        await http.get(BASE_URL + ENDPOINT_CATEGORIES, headers: headers);

    var categories = List<TodoCategory>();
    if (response.statusCode == 200) {
      var responseJson = jsonDecode(response.body);
      for (var item in responseJson) {
        categories.add(TodoCategory.fromJson(item));
      }
    }

    return categories;
  }

  void _fetchAndInsertNewCategories(
      List<TodoCategory> existingCategories) async {
    List<TodoCategory> apiCategories = await _getTodoCategories();

    var categoriesToAdd = List.from(apiCategories);

    for (var apiCategory in apiCategories) {
      for (var existingCategory in existingCategories) {
        if (existingCategory.categoryExternalId ==
            apiCategory.categoryExternalId) {
          categoriesToAdd.remove(apiCategory);
        }
      }
    }

    for (var categoryToAdd in categoriesToAdd) {
      await _db.insertCategory(categoryToAdd);
    }
  }

  Future<TodoPriority> _postTodoPriority(TodoPriority todoPriority) async {
    final response = await http.post(BASE_URL + ENDPOINT_PRIORITIES,
        headers: headers, body: jsonEncode(todoPriority.getMapForJson()));

    if (response.statusCode == 201) {
      var responseJson = json.decode(response.body);

      todoPriority.priorityExternalId = responseJson[TodoPriorityJson.id];
    }

    return todoPriority;
  }

  Future<bool> _deleteTodoPriority(TodoPriority todoPriority) async {
    final response = await http.delete(
        BASE_URL + ENDPOINT_PRIORITIES + "/${todoPriority.priorityExternalId}",
        headers: headers);

    return response.statusCode == 200 ||
        response.statusCode == 404; // deleting succeeded or item already gone?
  }

  Future<bool> _updateTodoPriority(TodoPriority todoPriority) async {
    final response = await http.put(
        BASE_URL + ENDPOINT_CATEGORIES + "/${todoPriority.priorityExternalId}",
        headers: headers,
        body: jsonEncode(todoPriority.getMapForJson()));

    return response.statusCode == 204 ||
        response.statusCode != 404; 
  }

  Future<List<TodoPriority>> _getTodoPriorities() async {
    final response =
        await http.get(BASE_URL + ENDPOINT_PRIORITIES, headers: headers);

    var priorities = List<TodoPriority>();
    if (response.statusCode == 200) {
      var responseJson = jsonDecode(response.body);
      for (var item in responseJson) {
        priorities.add(TodoPriority.fromJson(item));
      }
    }

    return priorities;
  }

  void _fetchAndInsertNewPriorities(List<TodoPriority> todoPriorities) async {
    List<TodoPriority> apiPriorities = await _getTodoPriorities();

    var prioritiesToAdd = List.from(apiPriorities);

    for (var apiPriority in apiPriorities) {
      for (var existingPriority in todoPriorities) {
        if (existingPriority.priorityExternalId ==
            apiPriority.priorityExternalId) {
          prioritiesToAdd.remove(apiPriority);
        }
      }
    }

    for (var priorityToAdd in prioritiesToAdd) {
      await _db.insertPriority(priorityToAdd);
    }
  }

  Future<TodoTask> _postTodoTask(TodoTask todoTask) async {
    final response = await http.post(BASE_URL + ENDPOINT_TASKS,
        headers: headers, body: jsonEncode(await todoTask.getMapForJson()));

    if (response.statusCode == 201) {
      var responseJson = json.decode(response.body);

      todoTask.taskExternalId = responseJson[TodoTaskJson.id];
    }

    return todoTask;
  }

  Future<bool> _deleteTodoTask(TodoTask todoTask) async {
    final response = await http.delete(
        BASE_URL + ENDPOINT_TASKS + "/${todoTask.taskExternalId}",
        headers: headers);

    return response.statusCode == 200 ||
        response.statusCode == 404; // deleting succeeded or item already gone?
  }

  Future<bool> _updateTodoTask(TodoTask todoTask) async {
    final response = await http.put(
        BASE_URL + ENDPOINT_TASKS + "/${todoTask.taskExternalId}",
        headers: headers,
        body: jsonEncode(todoTask.getMapForJson()));

    return response.statusCode == 204 ||
        response.statusCode != 404; 
  }

  Future<List<TodoTask>> _getTodoTasks() async {
    final response =
        await http.get(BASE_URL + ENDPOINT_TASKS, headers: headers);
    var todoTasks = List<TodoTask>();
    if (response.statusCode == 200) {
      var responseJson = jsonDecode(response.body);
      for (var item in responseJson) {
        int catId;
        await _db
            .getTodoCategoryByExternalId(item[TodoTaskJson.categoryId])
            .then((result) => catId = result.categoryId);

        int priorityId;
        await _db
            .getTodoPriorityByExternalId(item[TodoTaskJson.priorityId])
            .then((result) => priorityId = result.priorityId);

        item[TodoTaskJson.categoryId] = catId;
        item[TodoTaskJson.priorityId] = priorityId;
        todoTasks.add(TodoTask.fromJson(item));
      }
    }

    return todoTasks;
  }

  void _fetchAndInsertNewTasks(List<TodoTask> todoTasks) async {
    List<TodoTask> apiTasks = await _getTodoTasks();

    var tasksToAdd = List.from(apiTasks);

    for (var apiTask in apiTasks) {
      for (var existingTask in todoTasks) {
        if (existingTask.taskExternalId == apiTask.taskExternalId) {
          tasksToAdd.remove(apiTask);
        }
      }
    }

    for (var taskToAdd in tasksToAdd) {
      await _db.insertTask(taskToAdd);
    }
  }

  Future<JWT> login(AppUser appUser) async {
    final response = await http.post(BASE_URL + ENDPOINT_ACCOUNT_LOGIN,
        headers: headers, body: jsonEncode(appUser.getJson()));

    if (response.statusCode == 200) {
      return JWT.fromJson(json.decode(response.body));
    } else
      return JWT(token: '', status: response.reasonPhrase);
  }

  Future<JWT> register(AppUser appUser) async {
    final response = await http.post(BASE_URL + ENDPOINT_ACCOUNT_REGISTER,
        headers: headers, body: jsonEncode(appUser.getJson()));

    if (response.statusCode == 200) {
      return JWT.fromJson(json.decode(response.body));
    } else
      return JWT(token: '', status: response.reasonPhrase);
  }
}
