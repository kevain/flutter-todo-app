import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/providers/user_data_provider.dart';
import 'package:todo_app/screens/startup_navigator.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => TodoProvider()),
        ChangeNotifierProvider(create: (_) => UserDataProvider())
      ],
      child: MaterialApp(
        title: 'todoApp',
        theme:
            ThemeData(brightness: Brightness.light, primaryColor: Colors.blue),
        darkTheme: ThemeData(brightness: Brightness.dark),
        home: StartupNavigator(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
