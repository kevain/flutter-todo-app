import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/todo_category.dart';
import 'package:todo_app/providers/todo_provider.dart';

class TodoCategoryDialog extends StatefulWidget {
  final TodoCategory todoCategory;

  const TodoCategoryDialog({Key key, this.todoCategory}) : super(key: key);

  @override
  _TodoCategoryDialogState createState() => _TodoCategoryDialogState();
}

class _TodoCategoryDialogState extends State<TodoCategoryDialog> {
  TodoCategory get todoCategory => widget.todoCategory;

  final categoryFormKey = GlobalKey<FormState>();

  TextEditingController todoCategoryNameTextController;
  String categoryName = '';

  void categoryNameListener() {
    var controllerText = todoCategoryNameTextController.text.trim();
    categoryName = (controllerText.isNotEmpty ? controllerText : '');
  }

  void onDeleteButtonPressed(BuildContext context) {
    var todoCategoryProvider = Provider.of<TodoProvider>(context);

    todoCategory.categoryToBeDeleted = true;
    todoCategoryProvider.updateCategory(todoCategory);
  }

  void onSaveButtonPressed(BuildContext context) {
    var todoCategoryProvider = Provider.of<TodoProvider>(context);

    if (todoCategory == null) {
      var todoCategory = TodoCategory(categoryName: categoryName);

      todoCategoryProvider.insertCategory(todoCategory);
    } else {
      if (categoryName != null) {
        todoCategory.categoryName = categoryName;
      }

      todoCategoryProvider.updateCategory(todoCategory);
    }
  }

  @override
  void initState() {
    super.initState();
    todoCategoryNameTextController = TextEditingController(
        text: (todoCategory != null ? todoCategory.categoryName : ''));
    todoCategoryNameTextController.addListener(categoryNameListener);
  }

  @override
  void dispose() {
    todoCategoryNameTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        (todoCategory != null
            ? todoCategory.categoryName
            : 'Create New Category'),
        textAlign: TextAlign.center,
      ),
      titleTextStyle: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: Theme.of(context).accentColor),
      titlePadding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
      content: Form(
        key: categoryFormKey,
        autovalidate: true,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            TextFormField(
              controller: todoCategoryNameTextController,
              autofocus: (todoCategory == null),
              autovalidate: true,
              validator: TodoCategory.categoryNameValidator,
              decoration: InputDecoration(labelText: 'Category Name'),
            )
          ],
        ),
      ),
      contentPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
      actions: <Widget>[
        ButtonBar(
          alignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            RaisedButton(
              child: Text('Delete'),
              color: Colors.red,
              textColor: Colors.white,
              onPressed: (todoCategory == null
                  ? null
                  : () {
                      onDeleteButtonPressed(context);
                      Navigator.pop(context);
                    }),
            ),
            RaisedButton(
              child: Text('Save'),
              onPressed: () {
                final categoryForm = categoryFormKey.currentState;

                if (categoryForm.validate()) {
                  onSaveButtonPressed(context);
                  Navigator.pop(context);
                }
              },
            )
          ],
        )
      ],
      shape: RoundedRectangleBorder(
          side: BorderSide.none, borderRadius: BorderRadius.circular(10)),
    );
  }
}
