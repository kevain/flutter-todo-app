import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/todo_priority.dart';
import 'package:todo_app/providers/todo_provider.dart';

class TodoPriorityDialog extends StatefulWidget {
  final TodoPriority todoPriority;

  const TodoPriorityDialog({Key key, this.todoPriority}) : super(key: key);

  @override
  _TodoPriorityDialogState createState() => _TodoPriorityDialogState();
}

class _TodoPriorityDialogState extends State<TodoPriorityDialog> {
  TodoPriority get todoPriority => widget.todoPriority;

  final priorityFormKey = GlobalKey<FormState>();

  TextEditingController todoPriorityNameTextController;
  String priorityName = '';

  void priorityNameListener() {
    var controllerText = todoPriorityNameTextController.text.trim();
    priorityName = (controllerText.isNotEmpty ? controllerText : '');
  }

  void onDeleteButtonPressed(BuildContext context) {
    var provider = Provider.of<TodoProvider>(context);

    todoPriority.priorityToBeDeleted = true;
    provider.updateTodoPriority(todoPriority);
  }

  void onSaveButtonPressed(BuildContext context) {
    var provider = Provider.of<TodoProvider>(context);

    if (todoPriority == null) {
      var priority = TodoPriority(priorityName: priorityName, prioritySort: 0);

      provider.insertTodoPriority(priority);

    } else {
      if (priorityName != null) {
        todoPriority.priorityName = priorityName;
      }

      provider.updateTodoPriority(todoPriority);
    }
  }

  @override
  void initState() {
    super.initState();
    todoPriorityNameTextController = TextEditingController(
        text: (todoPriority != null ? todoPriority.priorityName : ''));
    todoPriorityNameTextController.addListener(priorityNameListener);
  }

  @override
  void dispose() {
    todoPriorityNameTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        (todoPriority != null
            ? todoPriority.priorityName
            : 'Create New Priority'),
        textAlign: TextAlign.center,
      ),
      titleTextStyle: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: Theme.of(context).accentColor),
      titlePadding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
      content: Form(
        key: priorityFormKey,
        autovalidate: true,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            TextFormField(
              controller: todoPriorityNameTextController,
              autofocus: (todoPriority == null),
              autovalidate: true,
              validator: TodoPriority.priorityNameValidator,
              decoration: InputDecoration(labelText: 'Priority Name'),
            )
          ],
        ),
      ),
      contentPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
      actions: <Widget>[
        ButtonBar(
          alignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            RaisedButton(
              child: Text('Delete'),
              color: Colors.red,
              textColor: Colors.white,
              onPressed: (todoPriority == null
                  ? null
                  : () {
                      onDeleteButtonPressed(context);
                      Navigator.pop(context);
                    }),
            ),
            RaisedButton(
              child: Text('Save'),
              onPressed: () {
                final priorityForm = priorityFormKey.currentState;

                if (priorityForm.validate()) {
                  onSaveButtonPressed(context);
                  Navigator.pop(context);
                }
              },
            )
          ],
        )
      ],
      shape: RoundedRectangleBorder(
          side: BorderSide.none, borderRadius: BorderRadius.circular(10)),
    );
  }
}
