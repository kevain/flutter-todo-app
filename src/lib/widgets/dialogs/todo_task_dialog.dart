import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/todo_category.dart';
import 'package:todo_app/models/todo_priority.dart';
import 'package:todo_app/models/todo_task.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/widgets/buttons/date_picker_button.dart';
import 'package:todo_app/widgets/dialogs/confirm_todo_task_deletion_dialog.dart';
import 'package:todo_app/widgets/dropdowns/category_dropdown_menu_builder.dart';
import 'package:todo_app/widgets/dropdowns/priority_dropdown_builder.dart';

class TodoTaskDialog extends StatefulWidget {
  final TodoTask todoTask;

  const TodoTaskDialog({Key key, this.todoTask}) : super(key: key);

  @override
  _TodoTaskDialogState createState() => _TodoTaskDialogState();
}

class _TodoTaskDialogState extends State<TodoTaskDialog> {
  TodoTask get todoTask => widget.todoTask;

  final taskFormKey = GlobalKey<FormState>();

  TextEditingController taskNameTextController;
  String taskName = '';

  TodoCategory todoCategory;
  TodoPriority todoPriority;
  DateTime todoTaskDueDate;

  void taskNameListener() {
    var controllerText = taskNameTextController.text.trim();
    taskName = (controllerText.isNotEmpty ? controllerText : '');
  }

  void onCategoryDropdownItemSelectionChange(TodoCategory selectedCategory) {
    setState(() {
      todoCategory = selectedCategory;
    });
  }

  void onPriorityDropdownItemSelectionChange(TodoPriority selectedPriority) {
    setState(() {
      todoPriority = selectedPriority;
    });
  }

  void onDateSelected(DateTime selectedDate) {
    setState(() {
      todoTaskDueDate = selectedDate;
    });
  }

  void onDeleteButtonPressed(BuildContext context) {
    var todoTaskProvider = Provider.of<TodoProvider>(context);
    todoTask.taskArchived = true;
    todoTaskProvider.updateTodoTask(todoTask);
  }

  void onSaveButtonPressed(BuildContext context) {
    var todoTaskProvider = Provider.of<TodoProvider>(context);

    if (todoTask == null) {
      var todoTask = TodoTask(
          taskName: taskName,
          taskCategoryId: todoCategory.categoryId,
          taskDue: todoTaskDueDate,
          taskSort: 0,
          taskCreated: Jiffy().local(),
          taskPriorityId: todoPriority.priorityId);

      todoTaskProvider.insertTodoTask(todoTask);
    } else {
      var updatedTodoTask = todoTask;

      if (TodoTask.taskNameValidator(taskName) == null) {
        updatedTodoTask.taskName = taskName;
      }

      if (todoTaskDueDate != null) {
        updatedTodoTask.taskDue = todoTaskDueDate;
      }

      if (todoCategory != null) {
        updatedTodoTask.taskCategoryId = todoCategory.categoryId;
      }

      if (todoPriority != null) {
        updatedTodoTask.taskPriorityId = todoPriority.priorityId;
      }

      todoTaskProvider.updateTodoTask(updatedTodoTask);
    }
  }

  @override
  void initState() {
    super.initState();
    taskNameTextController = TextEditingController(
        text: (todoTask != null ? todoTask.taskName : ''));
    taskNameTextController.addListener(taskNameListener);

    if (todoTask != null) {
      todoTaskDueDate = todoTask.taskDue;
    }
  }

  @override
  void dispose() {
    taskNameTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        (todoTask != null ? todoTask.taskName : 'Create New Task'),
        textAlign: TextAlign.center,
      ),
      titleTextStyle: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: Theme.of(context).accentColor),
      titlePadding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
      content: Form(
          key: taskFormKey,
          autovalidate: true,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              TextFormField(
                controller: taskNameTextController,
                autofocus: (todoTask == null),
                autovalidate: true,
                validator: TodoTask.taskNameValidator,
                decoration:
                    InputDecoration(labelText: 'What Would You Like To Do?'),
              ),
              CategoryDropdownMenuBuilder(
                selectedItemId: (todoTask != null
                    ? todoTask.taskCategoryId
                    : (todoCategory != null ? todoCategory.categoryId : null)),
                onSelectedCategoryChanged:
                    onCategoryDropdownItemSelectionChange,
              ),
              PriorityDropdownMenuBuilder(
                selectedItemId: (todoTask != null
                    ? todoTask.taskPriorityId
                    : (todoPriority != null ? todoPriority.priorityId : null)),
                onSelectedPriorityChanged:
                    onPriorityDropdownItemSelectionChange,
              ),
              DatePickerButton(
                selectedDate: todoTaskDueDate,
                onDateSelected: onDateSelected,
              )
            ],
          )),
      contentPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
      actions: <Widget>[
        ButtonBar(
          alignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            RaisedButton(
              child: Text('Delete'),
              color: Colors.red,
              textColor: Colors.white,
              onPressed: (todoTask == null
                  ? null
                  : () {
                      showDialog(
                          context: context,
                          builder: (_) => ConfirmTodoTaskDeletionDialog(
                                todoTask: todoTask,
                                onUserAgreed: () {
                                  todoTask.taskArchived = true;
                                  Provider.of<TodoProvider>(context)
                                      .updateTodoTask(todoTask);
                                  Navigator.pop(context);
                                },
                              ));
                    }),
            ),
            RaisedButton(
              child: Text('Save'),
              onPressed: () {
                final taskForm = taskFormKey.currentState;
                if (taskForm.validate() &&
                    (todoTask != null ||
                        todoCategory != null ||
                        todoPriority != null)) {
                  onSaveButtonPressed(context);
                  Navigator.pop(context);
                }
              },
            )
          ],
        )
      ],
      shape: RoundedRectangleBorder(
          side: BorderSide.none, borderRadius: BorderRadius.circular(10)),
    );
  }
}
