import 'package:flutter/material.dart';
import 'package:todo_app/models/todo_task.dart';

class ConfirmTodoTaskDeletionDialog extends StatelessWidget {
  final TodoTask todoTask;
  final Function onUserAgreed;

  const ConfirmTodoTaskDeletionDialog(
      {Key key, @required this.todoTask, @required this.onUserAgreed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Are you sure you want to remove this task?'),
      content: Text(todoTask.taskName),
      actions: <Widget>[
        ButtonBar(
          alignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            FlatButton(
              child: Text('No'),
              onPressed: () => Navigator.pop(context),
            ),
            RaisedButton(
              child: Text(
                'Yes',
                style: TextStyle(color: Colors.white),
              ),
              color: Colors.red,
              onPressed: () {
                Navigator.pop(context);
                onUserAgreed();
              },
            )
          ],
        )
      ],
    );
  }
}
