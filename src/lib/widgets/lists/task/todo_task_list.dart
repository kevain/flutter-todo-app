import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/todo_task.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/widgets/lists/task/todo_task_list_item.dart';

class TodoTaskList extends StatelessWidget {
  final List<TodoTask> todoTasks;

  const TodoTaskList(
      {Key key,
      @required this.todoTasks})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ReorderableListView(
      onReorder: (int oldIndex, int newIndex) {
        var task = todoTasks[oldIndex];
        task.taskSort = newIndex;
        Provider.of<TodoProvider>(context).updateTodoTask(task);
      },
      children: todoTasks
          .map((todoTask) => TodoTaskListItem(
                todoTask: todoTask
              ))
          .toList(),
    );
  }
}
