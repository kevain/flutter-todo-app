import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:jiffy/jiffy.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/todo_task.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/widgets/dialogs/confirm_todo_task_deletion_dialog.dart';
import 'package:todo_app/widgets/dialogs/todo_task_dialog.dart';

class TodoTaskListItem extends StatefulWidget {
  final TodoTask todoTask;

  const TodoTaskListItem({Key key, @required this.todoTask}) : super(key: key);

  @override
  Key get key => ValueKey('${todoTask.taskId}');

  @override
  _TodoTaskListItemState createState() => _TodoTaskListItemState();
}

class _TodoTaskListItemState extends State<TodoTaskListItem> {
  Widget createTileTitle() {
    if (widget.todoTask.taskCompleted) {
      return Text(
        widget.todoTask.taskName,
        style: TextStyle(
            decoration: TextDecoration.lineThrough,
            fontStyle: FontStyle.italic,
            fontSize: 16),
      );
    } else {
      return Text(
        widget.todoTask.taskName,
        style: TextStyle(fontSize: 18),
      );
    }
  }

  Widget createTileSubtitle() {
    if (widget.todoTask.taskDue == null) {
      return null;
    }

    if (widget.todoTask.taskCompleted) {
      return Text(
        Jiffy(widget.todoTask.taskDue).format("EEEE, MMMM do"),
        style: TextStyle(
            decoration: TextDecoration.lineThrough,
            fontStyle: FontStyle.italic),
      );
    }

    if (todoTaskDueCritical()) {
      return Text(
        Jiffy(widget.todoTask.taskDue).format("EEEE, MMMM do"),
        style: TextStyle(fontStyle: FontStyle.italic, color: Colors.red),
      );
    }

    return Text(
      Jiffy(widget.todoTask.taskDue).format("EEEE, MMMM do"),
      style: TextStyle(fontStyle: FontStyle.italic),
    );
  }

  bool todoTaskDueCritical() {
    var now = Jiffy().local();
    var taskDue = Jiffy(widget.todoTask.taskDue).local();

    return taskDue.isBefore(now);
  }

  void onCheckBoxTapped(bool newValue, BuildContext context) {
    widget.todoTask.taskCompleted = newValue;

    Provider.of<TodoProvider>(context).updateTodoTask(widget.todoTask);
    setState(() {}); // trigger widget update
  }

  void onUserAgreedWithTaskDeletion(BuildContext context) {
    widget.todoTask.taskArchived = true;
    Provider.of<TodoProvider>(context).updateTodoTask(widget.todoTask);
  }

  Widget createTileContent(BuildContext context) {
    return ListTile(
      leading: Checkbox(
        value: widget.todoTask.taskCompleted,
        onChanged: (newValue) => onCheckBoxTapped(newValue, context),
      ),
      title: createTileTitle(),
      subtitle: createTileSubtitle(),
      onTap: () => showDialog(
        context: context,
        builder: (_) => TodoTaskDialog(
          todoTask: widget.todoTask,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
      child: Slidable(
        key: ValueKey(
            '${widget.todoTask.taskId}/${widget.todoTask.taskCreated}'),
        actionPane: SlidableStrechActionPane(),
        child: Card(
          elevation: 7,
          child: createTileContent(context),
        ),
        secondaryActions: <Widget>[
          IconSlideAction(
            caption: 'Remove',
            color: Colors.red,
            icon: Icons.delete,
            onTap: () => showDialog(
                context: context,
                builder: (_) => ConfirmTodoTaskDeletionDialog(
                    todoTask: widget.todoTask,
                    onUserAgreed: () => onUserAgreedWithTaskDeletion(context))),
          )
        ],
        dismissal: SlidableDismissal(
          child: SlidableDrawerDismissal(),
          onWillDismiss: (_) => showDialog(
              context: context,
              builder: (_) => ConfirmTodoTaskDeletionDialog(
                    todoTask: widget.todoTask,
                    onUserAgreed: () => onUserAgreedWithTaskDeletion(context),
                  )),
          onDismissed: (_) {},
        ),
      ),
    );
  }
}
