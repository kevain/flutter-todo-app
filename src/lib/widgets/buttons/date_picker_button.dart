import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';

class DatePickerButton extends StatelessWidget {
  final DateTime selectedDate;
  final Function onDateSelected;

  const DatePickerButton(
      {Key key, this.selectedDate, @required this.onDateSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton.icon(
      label: (selectedDate == null
          ? Text('Select Due Date')
          : Text(Jiffy(selectedDate).format("EEEE, MMMM do"))),
      icon: Icon(Icons.calendar_view_day),
      onPressed: () async {
        var jiffyLocal = Jiffy().local();

        await showDatePicker(
            context: context,
            initialDate:
                (selectedDate != null ? Jiffy(selectedDate).local() : jiffyLocal),
            firstDate: jiffyLocal.subtract(Duration(days: 1)),
            lastDate: jiffyLocal.add(Duration(days: 365)),
            builder: (context, child) {
              return Theme(
                child: child,
                data: Theme.of(context),
              );
            }).then((value) => (value != null ? onDateSelected(value) : null));
      },
    );
  }
}
