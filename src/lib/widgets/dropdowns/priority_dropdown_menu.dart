import 'package:flutter/material.dart';
import 'package:todo_app/models/todo_priority.dart';

class PriorityDropDownMenu extends StatelessWidget {
  final List<TodoPriority> todoPriorities;
  final TodoPriority selectedItem;
  final ValueChanged<TodoPriority> onSelectedChanged;

  const PriorityDropDownMenu(
      {Key key,
      @required this.todoPriorities,
      @required this.onSelectedChanged,
      this.selectedItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButton(
      hint: Text('Select Priority'),
      disabledHint: Text('No Priorities!'),
      items: _getDropdownList(),
      value: selectedItem,
      onChanged: onSelectedChanged,
    );
  }

  List<DropdownMenuItem<TodoPriority>> _getDropdownList() {
    List<DropdownMenuItem<TodoPriority>> dropDown = List();

    todoPriorities.forEach(
      (priority) => dropDown.add(
        _getDropdownMenuItem(priority),
      ),
    );

    return dropDown;
  }

  DropdownMenuItem<TodoPriority> _getDropdownMenuItem(
      TodoPriority todoPriority) {
    return DropdownMenuItem(
      value: todoPriority,
      child: Text(todoPriority.priorityName),
    );
  }
}
