import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/todo_category.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/widgets/dropdowns/category_dropdown_menu.dart';

class CategoryDropdownMenuBuilder extends StatelessWidget {
  final int selectedItemId;
  final ValueChanged<TodoCategory> onSelectedCategoryChanged;

  const CategoryDropdownMenuBuilder(
      {Key key,
      @required this.selectedItemId,
      @required this.onSelectedCategoryChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<TodoProvider>(
      builder: (context, categoryProvider, _) => FutureBuilder(
        future: categoryProvider.todoCategories,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
            case ConnectionState.active:
              return DropdownButton(
                disabledHint: Text('Loading Categories ...'),
                items: null,
                onChanged: (value) {},
              );
            case ConnectionState.done:
              var categoriesList = snapshot.data as List<TodoCategory>;

              return CategoryDropDownMenu(
                todoCategories: categoriesList,
                selectedItem: (selectedItemId != null
                    ? categoriesList
                        .where((cat) => cat.categoryId == selectedItemId)
                        .first
                    : null),
                onSelectedChanged: onSelectedCategoryChanged,
              );
          }

          return null;
        },
      ),
    );
  }
}
