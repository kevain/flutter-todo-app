import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/todo_priority.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/widgets/dropdowns/priority_dropdown_menu.dart';

class PriorityDropdownMenuBuilder extends StatelessWidget {
  final int selectedItemId;
  final ValueChanged<TodoPriority> onSelectedPriorityChanged;

  const PriorityDropdownMenuBuilder(
      {Key key,
      @required this.selectedItemId,
      @required this.onSelectedPriorityChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<TodoProvider>(
      builder: (context, provider, _) => FutureBuilder(
        future: provider.todoPriorities,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
            case ConnectionState.active:
              return DropdownButton(
                disabledHint: Text('Loading Priorities ...'),
                items: null, 
                onChanged: (value) {},
              );
            case ConnectionState.done:
              var categoriesList = snapshot.data as List<TodoPriority>;

              return PriorityDropDownMenu(
                todoPriorities: categoriesList,
                selectedItem: (selectedItemId != null
                    ? categoriesList
                        .where((cat) => cat.priorityId == selectedItemId)
                        .first
                    : null),
                onSelectedChanged: onSelectedPriorityChanged,
              );
          }

          return null;
        },
      ),
    );
  }
}
