import 'package:flutter/material.dart';
import 'package:todo_app/models/todo_category.dart';

class CategoryDropDownMenu extends StatelessWidget {
  final List<TodoCategory> todoCategories;
  final TodoCategory selectedItem;
  final ValueChanged<TodoCategory> onSelectedChanged;

  const CategoryDropDownMenu(
      {Key key,
      @required this.todoCategories,
      @required this.onSelectedChanged,
      this.selectedItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButton(
      hint: Text('Select Category'),
      disabledHint: Text('No Categories!'),
      items: _getDropdownList(),
      value: selectedItem,
      onChanged: onSelectedChanged,
    );
  }

  List<DropdownMenuItem<TodoCategory>> _getDropdownList() {
    List<DropdownMenuItem<TodoCategory>> dropDown = List();

    todoCategories.forEach(
      (category) => dropDown.add(
        _getDropdownMenuItem(category),
      ),
    );

    return dropDown;
  }

  DropdownMenuItem<TodoCategory> _getDropdownMenuItem(
      TodoCategory todoCategory) {
    return DropdownMenuItem(
      value: todoCategory,
      child: Text(todoCategory.categoryName),
    );
  }
}
