import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/screens/todo_task_list_screen.dart';

class SidebarTodayUpcomingAllTasksTiles extends StatelessWidget {
  String _generateTaskCountString(int taskCount) {
    switch (taskCount) {
      case 0:
        return 'No tasks';

      case 1:
        return '1 task';

      default:
        return '$taskCount tasks';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TodoProvider>(
      builder: (BuildContext context, TodoProvider provider, Widget child) {
        return FutureBuilder(
          future: provider.taskCountsForTodayUpcomingAndAll,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
              case ConnectionState.active:
                return Column(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.today),
                      title: Text('Today'),
                      onTap: () => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (_) => TodoTaskViewScreen(
                              viewType: TodoTaskViewType.Today),
                        ),
                      ),
                    ),
                    ListTile(
                      leading: Icon(Icons.view_week),
                      title: Text('Upcoming'),
                      onTap: () => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (_) => TodoTaskViewScreen(
                            viewType: TodoTaskViewType.Upcoming,
                          ),
                        ),
                      ),
                    ),
                    ListTile(
                      leading: Icon(Icons.note),
                      title: Text('All tasks'),
                      onTap: () => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (_) => TodoTaskViewScreen(
                            viewType: TodoTaskViewType.All,
                          ),
                        ),
                      ),
                    ),
                  ],
                );

              case ConnectionState.done:
                var taskCounts = snapshot.data as Map<String, dynamic>;

                return Column(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.today),
                      title: Text('Today'),
                      subtitle: Text(
                        _generateTaskCountString(
                          taskCounts['TASKCOUNT_TODAY'],
                        ),
                      ),
                      onTap: () => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (_) => TodoTaskViewScreen(
                              viewType: TodoTaskViewType.Today),
                        ),
                      ),
                    ),
                    ListTile(
                      leading: Icon(Icons.view_week),
                      title: Text('Upcoming'),
                      subtitle: Text(
                        _generateTaskCountString(
                          taskCounts['TASKCOUNT_WEEK'],
                        ),
                      ),
                      onTap: () => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (_) => TodoTaskViewScreen(
                            viewType: TodoTaskViewType.Upcoming,
                          ),
                        ),
                      ),
                    ),
                    ListTile(
                      leading: Icon(Icons.note),
                      title: Text('All tasks'),
                      subtitle: Text(
                        _generateTaskCountString(
                          taskCounts['TASKCOUNT_ALL'],
                        ),
                      ),
                      onTap: () => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (_) => TodoTaskViewScreen(
                            viewType: TodoTaskViewType.All,
                          ),
                        ),
                      ),
                    ),
                  ],
                );
            }

            return null;
          },
        );
      },
    );
  }
}
