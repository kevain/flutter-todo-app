import 'package:flutter/material.dart';
import 'package:todo_app/datastore/DTO/todo_category_with_task_count.dart';
import 'package:todo_app/models/todo_category.dart';
import 'package:todo_app/screens/todo_task_list_screen.dart';
import 'package:todo_app/widgets/dialogs/todo_category_dialog.dart';

class SideBarCategoryTile extends StatelessWidget {
  @override
  Key get key => ValueKey('${category.categoryId}/${category.categoryName}');

  final TodoCategoryWithTaskCount categoryWithCount;

  int get taskCount => categoryWithCount.taskCount;
  TodoCategory get category => categoryWithCount.todoCategory;

  const SideBarCategoryTile({Key key, @required this.categoryWithCount})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(Icons.list),
      title: Text(category.categoryName),
      subtitle: Text(_generateTaskCountString()),
      onTap: () => Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => TodoTaskViewScreen(
            viewType: TodoTaskViewType.Category,
            todoCategory: category,
          ),
        ),
      ),
      onLongPress: () => showDialog(
          context: context,
          builder: (context) => TodoCategoryDialog(
                todoCategory: category,
              )),
    );
  }

  String _generateTaskCountString() {
    switch (taskCount) {
      case 0:
        return 'No tasks';

      case 1:
        return '1 task';

      default:
        return '$taskCount tasks';
    }
  }
}
