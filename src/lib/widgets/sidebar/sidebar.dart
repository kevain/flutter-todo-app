import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/providers/user_data_provider.dart';
import 'package:todo_app/screens/login_screen.dart';
import 'package:todo_app/screens/todo_category_list_screen.dart';
import 'package:todo_app/screens/todo_priority_list_screen.dart';
import 'package:todo_app/widgets/sidebar/sidebar_category_list.dart';
import 'package:todo_app/widgets/sidebar/sidebar_header.dart';
import 'package:todo_app/widgets/sidebar/sidebar_priority_list.dart';
import 'package:todo_app/widgets/sidebar/sidebar_today_upcoming_all_tasks_tiles.dart';

class SideBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      children: <Widget>[
        SideBarHeader(),
        SidebarTodayUpcomingAllTasksTiles(),
        Divider(
          thickness: 5,
        ),
        ListTile(
            leading: Icon(Icons.add),
            title: Text('Manage Categories'),
            onTap: () => Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (_) => TodoCategoryListScreen()))),
        Divider(),
        SideBarCategoryList(),
        Divider(
          thickness: 5,
        ),
        ListTile(
            leading: Icon(Icons.add),
            title: Text('Manage Priorities'),
            onTap: () => Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (_) => TodoPriorityListScreen()))),
        Divider(),
        SideBarPriorityList(),
        Divider(),
        ListTile(
          leading: Icon(Icons.lock),
          title: Text('Log out'),
          onTap: () {
            var provider = Provider.of<UserDataProvider>(context);
            provider.removeAppUser();
            Navigator.pop(context); // close sidebar
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (_) => LoginScreen())); // navigate to login screen
            provider.removeUserData();
          },
        )
      ],
    ));
  }
}
