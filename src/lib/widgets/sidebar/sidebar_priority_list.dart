import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/datastore/DTO/todo_priority_with_task_count.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/widgets/sidebar/sidebar_priority_tile.dart';

class SideBarPriorityList extends StatelessWidget {
  @override
  Key get key => ValueKey('SideBarPriorityList');

  @override
  Widget build(BuildContext context) {
    return Consumer<TodoProvider>(builder: (context, categoryProvider, _) {
      return FutureBuilder(
        future: categoryProvider.todoPrioritiesWithTaskCounts,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
            case ConnectionState.active:
              return Container();

            case ConnectionState.done:
              if (snapshot.hasError) {
                return Text('Error retrieving data');
              } else {
                var priorities =
                    snapshot.data as List<TodoPriorityWithTaskCount>;
                return Column(children: _getCategoryTiles(priorities));
              }
              break;
          }
          return null;
        },
      );
    });
  }

  List<Widget> _getCategoryTiles(List<TodoPriorityWithTaskCount> data) {
    List<Widget> tiles = List();

    data.forEach((priority) => tiles.add(
          SideBarPriorityTile(
            priorityWithTaskCount: priority,
          ),
        ));

    return tiles;
  }
}
