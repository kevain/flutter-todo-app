import 'package:flutter/material.dart';
import 'package:todo_app/datastore/DTO/todo_priority_with_task_count.dart';
import 'package:todo_app/models/todo_priority.dart';
import 'package:todo_app/screens/todo_task_list_screen.dart';
import 'package:todo_app/widgets/dialogs/todo_priority_dialog.dart';

class SideBarPriorityTile extends StatelessWidget {
  @override
  Key get key => ValueKey('${priority.priorityId}/${priority.priorityName}');

  final TodoPriorityWithTaskCount priorityWithTaskCount;

  int get taskCount => priorityWithTaskCount.taskCount;
  TodoPriority get priority => priorityWithTaskCount.todoPriority;

  const SideBarPriorityTile({Key key, @required this.priorityWithTaskCount})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(Icons.list),
      title: Text(priority.priorityName),
      subtitle: Text(_generateTaskCountString()),
      onTap: () => Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => TodoTaskViewScreen(
            viewType: TodoTaskViewType.Priority,
            todoPriority: priority,
          ),
        ),
      ),
      onLongPress: () => showDialog(
          context: context,
          builder: (context) => TodoPriorityDialog(
                todoPriority: priority,
              )),
    );
  }

  String _generateTaskCountString() {
    switch (taskCount) {
      case 0:
        return 'No tasks';

      case 1:
        return '1 task';

      default:
        return '$taskCount tasks';
    }
  }
}
