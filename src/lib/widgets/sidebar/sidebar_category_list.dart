import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/datastore/DTO/todo_category_with_task_count.dart';
import 'package:todo_app/providers/todo_provider.dart';
import 'package:todo_app/widgets/sidebar/sidebar_category_tile.dart';

class SideBarCategoryList extends StatelessWidget {
  @override
  Key get key => ValueKey('SideBarCategoryList');

  @override
  Widget build(BuildContext context) {
    return Consumer<TodoProvider>(
        builder: (context, categoryProvider, _) {
      return FutureBuilder(
        future: categoryProvider.todoCategoriesWithTaskCounts,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
            case ConnectionState.active:
              return Container();

            case ConnectionState.done:
              if (snapshot.hasError) {
                return Text('Error retrieving data');
              } else {
                var categories =
                    snapshot.data as List<TodoCategoryWithTaskCount>;
                return Column(children: _getCategoryTiles(categories));
              }
              break;
          }
          return null;
        },
      );
    });
  }

  List<Widget> _getCategoryTiles(List<TodoCategoryWithTaskCount> data) {
    List<Widget> tiles = List();

    data.forEach((category) => tiles.add(
          SideBarCategoryTile(
            categoryWithCount: category,
          ),
        ));

    return tiles;
  }
}
