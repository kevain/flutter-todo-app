import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/models/app_user.dart';
import 'package:todo_app/providers/user_data_provider.dart';

class SideBarHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<UserDataProvider>(
      builder: (BuildContext context, provider, Widget child) {
        return FutureBuilder(
          future: provider.appUser,
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
              case ConnectionState.active:
                return UserAccountsDrawerHeader(
                  currentAccountPicture: CircleAvatar(),
                  accountName: Text(''),
                  accountEmail: Text(''),
                );

              case ConnectionState.done:
                var userData = snapshot.data as AppUser;

                return UserAccountsDrawerHeader(
                  currentAccountPicture: CircleAvatar(
                    child: Text(
                      userData.email.substring(0, 1),
                      style: TextStyle(fontSize: 25),
                    ),
                  ),
                  accountEmail: Text(userData.email),
                  accountName: Text(
                    userData.email.substring(
                      0,
                      userData.email.indexOf('@'),
                    ),
                  ),
                );
            }
            return null;
          },
        );
      },
    );
  }
}
