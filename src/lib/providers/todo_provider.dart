import 'package:flutter/material.dart';
import 'package:todo_app/API/todo_api.dart';
import 'package:todo_app/datastore/DTO/todo_category_with_task_count.dart';
import 'package:todo_app/datastore/DTO/todo_priority_with_task_count.dart';
import 'package:todo_app/datastore/database_helper.dart';
import 'package:todo_app/models/todo_category.dart';
import 'package:todo_app/models/todo_priority.dart';
import 'package:todo_app/models/todo_task.dart';

class TodoProvider extends ChangeNotifier {
  final DatabaseHelper _db = DatabaseHelper();
  final TodoAPI _api = TodoAPI();

  void syncData() async {
    _api.syncData();
    notifyListeners();
  }

  // TODOCATEGORIES
  Future<List<TodoCategory>> get todoCategories async =>
      await _db.getAllCategories();

  void updateCategory(TodoCategory todoCategory) async {
    await _db.updateTodoCategory(todoCategory).then((_) => notifyListeners());
  }

  void removeCategory(TodoCategory todoCategory) async {
    await _db
        .removeCategory(todoCategory.categoryId)
        .then((_) => notifyListeners());
  }

  void insertCategory(TodoCategory todoCategory) async {
    await _db.insertCategory(todoCategory).then((_) => notifyListeners());
  }

  Future<List<TodoTask>> todoTasks(String where) async {
    return await _db.getAllTodoTasks(where);
  }

  Future<List<TodoTask>> todoTasksForCategory(
      TodoCategory category, String filter) async {
    return await _db.getTodoTasksByCategoryId(category.categoryId, filter);
  }

  Future<List<TodoTask>> todoTasksForToday(String where) async {
    return await _db.getTodoTasksForToday(where);
  }

  Future<List<TodoTask>> todoTasksForUpcomingWeek(String where) async {
    return await _db.getUpcomingTodoTasks(where);
  }

  void updateTodoTask(TodoTask todoTask) async {
    await _db.updateTodoTask(todoTask).then((_) => notifyListeners());
  }

  void removeTodoTask(TodoTask todoTask) async {
    await _db.removeTodoTask(todoTask.taskId).then((_) => notifyListeners());
  }

  void insertTodoTask(TodoTask todoTask) async {
    await _db.insertTask(todoTask).then((_) => notifyListeners());
  }

  void updateTaskCompletion(TodoTask todoTask) async {
    await _db.updateTodoTask(todoTask).then((_) => notifyListeners());
  }

  Future<List<TodoPriority>> get todoPriorities async =>
      await _db.getAllTodoPriorities();

  void insertTodoPriority(TodoPriority todoPriority) async {
    await _db.insertPriority(todoPriority).then((_) => notifyListeners());
  }

  void updateTodoPriority(TodoPriority todoPriority) async {
    await _db.updateTodoPriority(todoPriority).then((_) => notifyListeners());
  }

  void removeTodoPriority(int id) async {
    await _db.removePriority(id);
  }

  Future<List<TodoCategoryWithTaskCount>>
      get todoCategoriesWithTaskCounts async =>
          await _db.getTodoCategoriesWithTaskCounts();
  Future<List<TodoPriorityWithTaskCount>>
      get todoPrioritiesWithTaskCounts async =>
          await _db.getTodoPrioritiesWithTaskCounts();

  Future<List<TodoTask>> todoTasksForPriority(
      TodoPriority todoPriority, String where) async {
    return await _db.getTodoTasksByPriority(todoPriority, where);
  }

  Future<Map<String, dynamic>> get taskCountsForTodayUpcomingAndAll async =>
      await _db.getUncompletedTaskCountsForTodayUpcomingAndAll();
}
