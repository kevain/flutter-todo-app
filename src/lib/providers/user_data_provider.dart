import 'package:flutter/material.dart';
import 'package:todo_app/API/todo_api.dart';
import 'package:todo_app/datastore/database_helper.dart';
import 'package:todo_app/models/app_user.dart';

class UserDataProvider extends ChangeNotifier {
  final DatabaseHelper _db = DatabaseHelper();
  final TodoAPI _api = TodoAPI();

  Future<AppUser> get appUser async => await _db.getAppUser();

  void addAppUser(AppUser appUser) async {
    await _db.insertAppUser(appUser);
  }

  void removeAppUser() async {
    await _db.removeAppUser();
  }

  void removeUserData() async {
    _db.removeUserData();
  }
}
